<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class QuestionsMigration_101
 */
class QuestionsMigration_101 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'questions', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'autoIncrement' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'question_name', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 100,
								'after' => 'id' 
						) ),
						new Column ( 'question_content', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 4000,
								'after' => 'question_name' 
						) ),
						new Column ( 'question_type', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'question_content' 
						) ),
						new Column ( 'difficulty', array (
								'type' => Column::TYPE_INTEGER,
								'size' => 11,
								'after' => 'question_type' 
						) ),
						new Column ( 'author_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'difficulty' 
						) ),
						new Column ( 'date_modify', array (
								'type' => Column::TYPE_DATETIME,
								'size' => 1,
								'after' => 'author_id' 
						) ),
						new Column ( 'subject_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'date_modify' 
						) ) 
				),
				'indexes' => array (
						new Index ( 'PRIMARY', array (
								'id' 
						) ),
						new Index ( 'question_type', array (
								'question_type' 
						) ),
						new Index ( 'subject_id', array (
								'subject_id' 
						) ),
						new Index ( 'author_id', array (
								'author_id' 
						) ) 
				),
				'references' => array (
						new Reference ( 'questions_ibfk_1', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'categories',
								'columns' => array (
										'question_type' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ),
						new Reference ( 'questions_ibfk_2', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'subjects',
								'columns' => array (
										'subject_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ),
						new Reference ( 'questions_ibfk_3', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'users',
								'columns' => array (
										'author_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'BASE TABLE',
						'AUTO_INCREMENT' => '6',
						'ENGINE' => 'InnoDB',
						'TABLE_COLLATION' => 'utf8_general_ci' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
