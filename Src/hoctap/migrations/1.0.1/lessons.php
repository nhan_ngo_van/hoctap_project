<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class LessonsMigration_101
 */
class LessonsMigration_101 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'lessons', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'autoIncrement' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'name', array (
								'type' => Column::TYPE_VARCHAR,
								'notNull' => true,
								'size' => 200,
								'after' => 'id' 
						) ),
						new Column ( 'content', array (
								'type' => Column::TYPE_TEXT,
								'notNull' => true,
								'size' => 1,
								'after' => 'name' 
						) ),
						new Column ( 'category_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'content' 
						) ),
						new Column ( 'subject_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'category_id' 
						) ) 
				),
				'indexes' => array (
						new Index ( 'PRIMARY', array (
								'id' 
						) ),
						new Index ( 'category_id', array (
								'category_id' 
						) ),
						new Index ( 'subject_id', array (
								'subject_id' 
						) ) 
				),
				'references' => array (
						new Reference ( 'lessons_ibfk_1', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'categories',
								'columns' => array (
										'category_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ),
						new Reference ( 'lessons_ibfk_2', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'subjects',
								'columns' => array (
										'subject_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'BASE TABLE',
						'AUTO_INCREMENT' => '1',
						'ENGINE' => 'InnoDB',
						'TABLE_COLLATION' => 'utf8_general_ci' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
