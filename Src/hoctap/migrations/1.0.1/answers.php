<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class AnswersMigration_101
 */
class AnswersMigration_101 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'answers', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'autoIncrement' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'date_modify', array (
								'type' => Column::TYPE_DATETIME,
								'notNull' => true,
								'size' => 1,
								'after' => 'id' 
						) ),
						new Column ( 'answer_content', array (
								'type' => Column::TYPE_VARCHAR,
								'notNull' => true,
								'size' => 5000,
								'after' => 'date_modify' 
						) ),
						new Column ( 'author_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'answer_content' 
						) ) 
				),
				'indexes' => array (
						new Index ( 'PRIMARY', array (
								'id' 
						) ),
						new Index ( 'author_id', array (
								'author_id' 
						) ) 
				),
				'references' => array (
						new Reference ( 'answers_ibfk_1', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'users',
								'columns' => array (
										'author_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'BASE TABLE',
						'AUTO_INCREMENT' => '1',
						'ENGINE' => 'InnoDB',
						'TABLE_COLLATION' => 'utf8_general_ci' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
