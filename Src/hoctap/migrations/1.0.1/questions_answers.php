<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class QuestionsAnswersMigration_101
 */
class QuestionsAnswersMigration_101 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'questions_answers', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'autoIncrement' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'question_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'id' 
						) ),
						new Column ( 'answers_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'question_id' 
						) ) 
				),
				'indexes' => array (
						new Index ( 'PRIMARY', array (
								'id' 
						) ),
						new Index ( 'question_id', array (
								'question_id' 
						) ),
						new Index ( 'answers_id', array (
								'answers_id' 
						) ) 
				),
				'references' => array (
						new Reference ( 'questions_answers_ibfk_1', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'questions',
								'columns' => array (
										'question_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ),
						new Reference ( 'questions_answers_ibfk_2', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'answers',
								'columns' => array (
										'answers_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'BASE TABLE',
						'AUTO_INCREMENT' => '1',
						'ENGINE' => 'InnoDB',
						'TABLE_COLLATION' => 'utf8_general_ci' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
