<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class LessonsMigration_100
 */
class LessonsMigration_100 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'lessons', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'autoIncrement' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'name', array (
								'type' => Column::TYPE_VARCHAR,
								'notNull' => true,
								'size' => 200,
								'after' => 'id' 
						) ),
						new Column ( 'content', array (
								'type' => Column::TYPE_TEXT,
								'notNull' => true,
								'size' => 1,
								'after' => 'name' 
						) ),
						new Column ( 'category_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'content' 
						) ),
						new Column ( 'subject_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'category_id' 
						) ),
						new Column ( 'author_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'subject_id' 
						) ),
						new Column ( 'created_date', array (
								'type' => Column::TYPE_DATETIME,
								'notNull' => true,
								'size' => 1,
								'after' => 'author_id' 
						) ),
						new Column ( 'date_modify', array (
								'type' => Column::TYPE_DATETIME,
								'notNull' => true,
								'size' => 1,
								'after' => 'created_date' 
						) ),
						new Column ( 'active_view', array (
								'type' => Column::TYPE_DATETIME,
								'size' => 1,
								'after' => 'date_modify' 
						) ) 
				),
				'indexes' => array (
						new Index ( 'PRIMARY', array (
								'id' 
						) ),
						new Index ( 'category_id', array (
								'category_id' 
						) ),
						new Index ( 'subject_id', array (
								'subject_id' 
						) ),
						new Index ( 'author_id', array (
								'author_id' 
						) ) 
				),
				'references' => array (
						new Reference ( 'lessons_ibfk_1', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'categories',
								'columns' => array (
										'category_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ),
						new Reference ( 'lessons_ibfk_2', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'subjects',
								'columns' => array (
										'subject_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ),
						new Reference ( 'lessons_ibfk_3', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'users',
								'columns' => array (
										'author_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'BASE TABLE',
						'AUTO_INCREMENT' => '3',
						'ENGINE' => 'InnoDB',
						'TABLE_COLLATION' => 'utf8_general_ci' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
