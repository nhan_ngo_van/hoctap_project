<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class AnswersMigration_100
 */
class AnswersMigration_100 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'answers', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'autoIncrement' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'created_date', array (
								'type' => Column::TYPE_DATETIME,
								'notNull' => true,
								'size' => 1,
								'after' => 'id' 
						) ),
						new Column ( 'date_modify', array (
								'type' => Column::TYPE_DATETIME,
								'notNull' => true,
								'size' => 1,
								'after' => 'created_date' 
						) ),
						new Column ( 'answer_content', array (
								'type' => Column::TYPE_VARCHAR,
								'notNull' => true,
								'size' => 5000,
								'after' => 'date_modify' 
						) ),
						new Column ( 'author_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'answer_content' 
						) ),
						new Column ( 'question_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'author_id' 
						) ),
						new Column ( 'active_view', array (
								'type' => Column::TYPE_INTEGER,
								'default' => '1',
								'notNull' => true,
								'size' => 11,
								'after' => 'question_id' 
						) ) 
				),
				'indexes' => array (
						new Index ( 'PRIMARY', array (
								'id' 
						) ),
						new Index ( 'question_id', array (
								'question_id' 
						) ),
						new Index ( 'author_id', array (
								'author_id' 
						) ) 
				),
				'references' => array (
						new Reference ( 'answers_ibfk_1', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'questions',
								'columns' => array (
										'question_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ),
						new Reference ( 'answers_ibfk_2', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'users',
								'columns' => array (
										'author_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'BASE TABLE',
						'AUTO_INCREMENT' => '1',
						'ENGINE' => 'InnoDB',
						'TABLE_COLLATION' => 'utf8_general_ci' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
