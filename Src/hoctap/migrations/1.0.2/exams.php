<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class ExamsMigration_100
 */
class ExamsMigration_100 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'exams', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'autoIncrement' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'name', array (
								'type' => Column::TYPE_VARCHAR,
								'notNull' => true,
								'size' => 200,
								'after' => 'id' 
						) ),
						new Column ( 'description', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 2000,
								'after' => 'name' 
						) ),
						new Column ( 'created_date', array (
								'type' => Column::TYPE_DATETIME,
								'notNull' => true,
								'size' => 1,
								'after' => 'description' 
						) ),
						new Column ( 'date_modify', array (
								'type' => Column::TYPE_DATETIME,
								'size' => 1,
								'after' => 'created_date' 
						) ),
						new Column ( 'author_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'date_modify' 
						) ),
						new Column ( 'footer', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 300,
								'after' => 'author_id' 
						) ),
						new Column ( 'active_view', array (
								'type' => Column::TYPE_DATETIME,
								'default' => '0000-00-00 00:00:00',
								'size' => 1,
								'after' => 'footer' 
						) ) 
				),
				'indexes' => array (
						new Index ( 'PRIMARY', array (
								'id' 
						) ),
						new Index ( 'author_id', array (
								'author_id' 
						) ) 
				),
				'references' => array (
						new Reference ( 'exams_ibfk_1', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'users',
								'columns' => array (
										'author_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'BASE TABLE',
						'AUTO_INCREMENT' => '3',
						'ENGINE' => 'InnoDB',
						'TABLE_COLLATION' => 'utf8_general_ci' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
