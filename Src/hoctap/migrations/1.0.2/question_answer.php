<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class QuestionAnswerMigration_100
 */
class QuestionAnswerMigration_100 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'question_answer', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'default' => '0',
								'notNull' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'exam_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'id' 
						) ),
						new Column ( 'question_number', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'exam_id' 
						) ),
						new Column ( 'question_desc', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 300,
								'after' => 'question_number' 
						) ),
						new Column ( 'question_id', array (
								'type' => Column::TYPE_INTEGER,
								'default' => '0',
								'notNull' => true,
								'size' => 11,
								'after' => 'question_desc' 
						) ),
						new Column ( 'question_name', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 100,
								'after' => 'question_id' 
						) ),
						new Column ( 'question_content', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 4000,
								'after' => 'question_name' 
						) ),
						new Column ( 'question_type', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'question_content' 
						) ),
						new Column ( 'difficulty', array (
								'type' => Column::TYPE_INTEGER,
								'size' => 11,
								'after' => 'question_type' 
						) ),
						new Column ( 'answer_content', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 5000,
								'after' => 'difficulty' 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'VIEW',
						'AUTO_INCREMENT' => '',
						'ENGINE' => '',
						'TABLE_COLLATION' => '' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
