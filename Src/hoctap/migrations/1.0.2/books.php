<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class BooksMigration_100
 */
class BooksMigration_100 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'books', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'autoIncrement' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'name', array (
								'type' => Column::TYPE_VARCHAR,
								'notNull' => true,
								'size' => 500,
								'after' => 'id' 
						) ),
						new Column ( 'description', array (
								'type' => Column::TYPE_VARCHAR,
								'notNull' => true,
								'size' => 4000,
								'after' => 'name' 
						) ),
						new Column ( 'lesson_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'description' 
						) ),
						new Column ( 'page_number', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'lesson_id' 
						) ),
						new Column ( 'author_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'page_number' 
						) ),
						new Column ( 'created_date', array (
								'type' => Column::TYPE_DATE,
								'size' => 1,
								'after' => 'author_id' 
						) ),
						new Column ( 'date_modify', array (
								'type' => Column::TYPE_DATE,
								'size' => 1,
								'after' => 'created_date' 
						) ) 
				),
				'indexes' => array (
						new Index ( 'PRIMARY', array (
								'id' 
						) ),
						new Index ( 'lesson_id', array (
								'lesson_id' 
						) ),
						new Index ( 'author_id', array (
								'author_id' 
						) ) 
				),
				'references' => array (
						new Reference ( 'books_ibfk_1', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'lessons',
								'columns' => array (
										'lesson_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ),
						new Reference ( 'books_ibfk_2', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'users',
								'columns' => array (
										'author_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'BASE TABLE',
						'AUTO_INCREMENT' => '1',
						'ENGINE' => 'InnoDB',
						'TABLE_COLLATION' => 'utf8_general_ci' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
