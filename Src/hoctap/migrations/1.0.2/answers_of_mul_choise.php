<?php
use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class AnswersOfMulChoiseMigration_100
 */
class AnswersOfMulChoiseMigration_100 extends Migration {
	/**
	 * Define the table structure
	 *
	 * @return void
	 */
	public function morph() {
		$this->morphTable ( 'answers_of_mul_choise', array (
				'columns' => array (
						new Column ( 'id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'autoIncrement' => true,
								'size' => 11,
								'first' => true 
						) ),
						new Column ( 'question_id', array (
								'type' => Column::TYPE_INTEGER,
								'notNull' => true,
								'size' => 11,
								'after' => 'id' 
						) ),
						new Column ( 'option_a', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 500,
								'after' => 'question_id' 
						) ),
						new Column ( 'option_b', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 500,
								'after' => 'option_a' 
						) ),
						new Column ( 'option_c', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 500,
								'after' => 'option_b' 
						) ),
						new Column ( 'option_d', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 500,
								'after' => 'option_c' 
						) ),
						new Column ( 'option_e', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 500,
								'after' => 'option_d' 
						) ),
						new Column ( 'true_answer', array (
								'type' => Column::TYPE_VARCHAR,
								'size' => 500,
								'after' => 'option_e' 
						) ) 
				),
				'indexes' => array (
						new Index ( 'PRIMARY', array (
								'id' 
						) ),
						new Index ( 'question_id', array (
								'question_id' 
						) ) 
				),
				'references' => array (
						new Reference ( 'answers_of_mul_choise_ibfk_1', array (
								'referencedSchema' => 'hoctap',
								'referencedTable' => 'questions',
								'columns' => array (
										'question_id' 
								),
								'referencedColumns' => array (
										'id' 
								) 
						) ) 
				),
				'options' => array (
						'TABLE_TYPE' => 'BASE TABLE',
						'AUTO_INCREMENT' => '1',
						'ENGINE' => 'InnoDB',
						'TABLE_COLLATION' => 'utf8_general_ci' 
				) 
		) );
	}
	
	/**
	 * Run the migrations
	 *
	 * @return void
	 */
	public function up() {
	}
	
	/**
	 * Reverse the migrations
	 *
	 * @return void
	 */
	public function down() {
	}
}
