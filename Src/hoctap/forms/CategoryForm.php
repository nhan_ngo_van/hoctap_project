<?php

namespace hoctap\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Submit;
use hoctap\Models\Subjects;
use hoctap\Models\Categories;
use Phalcon\Forms\Element\Hidden;

class CategoryForm extends Form {
	public function initialize(Categories $question = null, $options = null) {
		$this->setAction ( 'create' );
		if (isset ( $options ['edit'] ) && $options ['edit']) {
			$id = new Hidden ( 'id' );
			$this->add ( $id );
			$this->add ( new Submit ( "submit", array (
					'value' => 'Cập nhật',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		} else {
			$this->add ( new Submit ( "submit", array (
					'value' => 'Thêm',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		}
		$acronym = new Text ( 'acronym', array (
				'class' => 'form-control' 
		) );
		$type = new Text ( 'type', array (
				'class' => 'form-control' 
		) );
		$subject = new Select ( "subject_id", Subjects::find (), array (
				'using' => array (
						'id',
						'name' 
				),
				'class' => 'form-control' 
		) );
		$this->add ( $acronym );
		$this->add ( $type );
		$this->add ( $subject );
	}
}