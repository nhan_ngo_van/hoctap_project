<?php

namespace hoctap\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;

class QuestionSearchForm extends Form {
	public function initialize($numberOfQuestion) {
		$search = new Text ( 'search_value', array (
				'onkeyup' => 'searchQuestion(this.value)' 
		) );
		$submit = new Submit ( 'search' );
		$this->add ( $search );
		$this->add ( $submit );
	}
}