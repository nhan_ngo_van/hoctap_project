<?php

namespace hoctap\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;

class InfoForm extends Form {
	public function initialize($question = null, $options = null) {
		$this->setAction ( 'create' );
		if (isset ( $options ['edit'] ) && $options ['edit']) {
			$id = new Hidden ( 'id' );
			$this->add ( $id );
			$this->add ( new Submit ( "submit", array (
					'value' => 'Cập nhật',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		} else {
			$this->add ( new Submit ( "submit", array (
					'value' => 'Thêm',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		}
		$this->add ( new Text ( "tag", array (
				'class' => 'form-control',
				'placeholder' => 'Tên tag',
				'maxlength' => '100',
				'required' => 'required' 
		) ) );
		$this->add ( new TextArea( "content", array (
				'class' => 'form-control',
				'placeholder' => 'Nội dung',
				'maxlength' => '2000',
				'required' => 'required'
		) ) );
		
	}
}