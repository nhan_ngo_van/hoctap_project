<?php

namespace hoctap\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Hidden;

class UserTypeForm extends Form {
	public function initialize($userType = null, $options = null) {
		$this->setAction ( 'create' );
		if (isset ( $options ['edit'] ) && $options ['edit']) {
			$id = new Hidden ( 'id' );
			$this->add ( $id );
			$this->add ( new Submit ( "submit", array (
					'value' => 'Cập nhật',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		} else {
			$this->add ( new Submit ( "submit", array (
					'value' => 'Thêm',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		}
		$name = new Text ( 'name', array (
				'class' => 'form-control' 
		) );
		$module = new Text ( 'module', array (
				'class' => 'form-control' 
		) );
		$this->add ( $name );
		$this->add ( $module );
	}
}