<?php

namespace hoctap\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Email;
use Phalcon\Forms\Element\Numeric;
use Phalcon\Forms\Element\Select;
use hoctap\Models\UserTypes;

class UserForm extends Form {
	public function initialize($user = null, $options = null) {
		// $this->setAction ( 'update' );
		if (isset ( $options ['edit'] ) && $options ['edit']) {
			$id = new Hidden ( 'id' );
			$this->add ( $id );
			$this->add ( new Submit ( "submit", array (
					'value' => 'Cập nhật',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		} else {
			$this->add ( new Submit ( "submit", array (
					'value' => 'Thêm',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		}
		$first_name = new Text ( 'first_name', array (
				'class' => 'form-control',
				'required' => 'required' 
		) );
		$last_name = new Text ( 'last_name', array (
				'class' => 'form-control' 
		) );
		$user_name = new Text ( 'user_name', array (
				'class' => 'form-control',
				'required' => 'required' 
		) );
		$password = new Password ( 'password', array (
				'class' => 'form-control',
				'required' => 'required' 
		) );
		$mobile = new Text ( 'mobile_phone', array (
				'class' => 'form-control' 
		) );
		$bithday = new Date ( 'bithday', array (
				'class' => 'form-control' 
		) );
		$email = new Email ( 'email', array (
				'class' => 'form-control',
				'required' => 'required' 
		) );
		$facebook = new Text ( 'facebook', array (
				'class' => 'form-control' 
		) );
		$address = new Text ( 'address', array (
				'class' => 'form-control' 
		) );
		$school = new Text ( 'school', array (
				'class' => 'form-control' 
		) );
		$education_level = new Numeric ( 'education_level', array (
				'class' => 'form-control',
				'min' => '1',
				'max' => '18' 
		) );
		$user_type = new Select ( "user_type", UserTypes::find (), array (
				'using' => array (
						'id',
						'name' 
				),
				'class' => 'form-control' 
		) );
		$active = new Select ( 'active', array (
				0 => 'Chưa hoạt',
				1 => 'Đã kích hoạt' 
		), array (
				'using' => array (
						'id',
						'name' 
				),
				'class' => 'form-control' 
		) );
		$active->setLabel ( 'Trạng thái (active)' );
		$this->add ( $first_name );
		$this->add ( $last_name );
		$this->add ( $user_name );
		$this->add ( $password );
		$this->add ( $mobile );
		$this->add ( $bithday );
		$this->add ( $email );
		$this->add ( $facebook );
		$this->add ( $address );
		$this->add ( $school );
		$this->add ( $education_level );
		$this->add ( $user_type );
		$this->add ( $active );
	}
}