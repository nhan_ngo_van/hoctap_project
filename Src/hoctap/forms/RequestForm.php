<?php

namespace hoctap\Forms;

use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Form;

class RequestForm extends Form {
	public function initialize($request = null, $options = null) {
		$createDate = date ( 'Y-m-d H:m:s' );
		$this->setAction ( 'create' );
		if (isset ( $options ['edit'] ) && $options ['edit']) {
			$id = new Hidden ( 'id' );
			$this->add ( $id );
			$this->add ( new Submit ( "submit", array (
					'value' => 'Cập nhật',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		} else {
			$this->add ( new Submit ( "submit", array (
					'value' => 'Gửi',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		}
		$this->add ( new Text ( "title", array (
				'class' => 'form-control',
				'placeholder' => 'Tiêu đề',
				'maxlength' => '200',
				'required' => 'required' 
		) ) );
		$this->add ( new TextArea ( "content", array (
				'onchange' => 'updateFormula(this.value)',
				'placeholder' => 'Nội dung',
				'required' => 'required' 
		) ) );
	}
}