<?php

namespace hoctap\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;

class AnswerForm extends Form {
	public function initialize($answer = null, $options = null) {
		$this->setAction ( 'create' );
		if (isset ( $options ['edit'] ) && $options ['edit']) {
			$id = new Hidden ( 'id' );
			$this->add ( $id );
			$this->add ( new Submit ( "submit", array (
					'value' => 'Cập nhật',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		} else {
			$this->add ( new Submit ( "submit", array (
					'value' => 'Thêm',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		}
		if (isset ( $options ['question_id'] )) {
			$question_id = new Hidden ( 'question_id', array (
					'value' => $options ['question_id'] 
			) );
			$this->add ( $question_id );
		}
		$this->add ( new TextArea ( "answer_content", array (
				'onchange' => 'updateFormula(this.value)',
				'placeholder' => 'Nội dung',
				'required' => 'required' 
		) ) );
	}
}