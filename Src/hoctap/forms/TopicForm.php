<?php

namespace hoctap\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Hidden;

class TopicForm extends Form {
	public function initialize($topic = null, $options = null) {
		$this->setAction ( 'create' );
		if (isset ( $options ['edit'] ) && $options ['edit']) {
			$id = new Hidden ( 'id' );
			$this->add ( $id );
			$this->add ( new Submit ( "submit", array (
					'value' => 'Cập nhật',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		} else {
			$this->add ( new Submit ( "submit", array (
					'value' => 'Thêm',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		}
		$this->add ( new Text ( "name", array (
				'class' => 'form-control',
				'placeholder' => 'Tên topic',
				'maxlength' => '100',
				'required' => 'required' 
		) ) );
		$this->add ( new TextArea ( "content", array (
				'onchange' => 'updateFormula(this.value)',
				'placeholder' => 'Nội dung',
				'required' => 'required' 
		) ) );
	}
}