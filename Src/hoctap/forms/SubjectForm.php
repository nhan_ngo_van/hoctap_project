<?php

namespace hoctap\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use hoctap\Admin\Models\Subjects;
use Phalcon\Forms\Element\Hidden;

class SubjectForm extends Form {
	public function initialize(Subjects $subject = null, $options = null) {
		$this->setAction ( 'create' );
		if (isset ( $options ['edit'] ) && $options ['edit']) {
			$id = new Hidden ( 'id' );
			$this->add ( $id );
			$this->add ( new Submit ( "submit", array (
					'value' => 'Cập nhật',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		} else {
			$this->add ( new Submit ( "submit", array (
					'value' => 'Thêm',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		}
		$acronym = new Text ( 'acronym', array (
				'class' => 'form-control' 
		) );
		$name = new Text ( 'name', array (
				'class' => 'form-control' 
		) );
		$this->add ( $acronym );
		$this->add ( $name );
	}
}