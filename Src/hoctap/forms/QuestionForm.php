<?php

namespace hoctap\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Numeric;
use hoctap\Teacher\Models\Categories;
use hoctap\Teacher\Models\Subjects;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Date;

class QuestionForm extends Form {
	public function initialize($question = null, $options = null) {
		$activeDate = date ( 'Y-m-d H:m:s' );
		$this->setAction ( 'create' );
		if (isset ( $options ['edit'] ) && $options ['edit']) {
			$id = new Hidden ( 'id' );
			$this->add ( $id );
			$this->add ( new Submit ( "submit", array (
					'value' => 'Cập nhật',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		} else {
			$this->add ( new Submit ( "submit", array (
					'value' => 'Thêm',
					'class' => 'btn btn-primary btn-sm btn-block' 
			) ) );
		}
		$this->add ( new Text ( "question_name", array (
				'class' => 'form-control',
				'placeholder' => 'Tên câu hỏi',
				'maxlength' => '100',
				'required' => 'required' 
		) ) );
		$this->add ( new Select ( "question_type", Categories::find (), array (
				'using' => array (
						'id',
						'type' 
				),
				'class' => 'form-control' 
		) ) );
		$this->add ( new Select ( "subject_id", Subjects::find (), array (
				'using' => array (
						'id',
						'name' 
				),
				'class' => 'form-control' 
		) ) );
		$this->add ( new Numeric ( "difficulty", array (
				'class' => 'form-control',
				'max' => '10',
				'min' => '0',
				'value' => '5',
				'required' => 'required' 
		) ) );
		$this->add ( new TextArea ( "question_content", array (
				'onchange' => 'updateFormula(this.value)',
				'placeholder' => 'Nội dung',
				'required' => 'required' 
		) ) );
		
		$this->add ( new Date ( "active_view", array (
				'class' => 'form-control',
				'value' => $activeDate 
		) ) );
	}
}