<?php

namespace hoctap\Anonymous\Models;

class UserTypes extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	protected $id;
	
	/**
	 *
	 * @var string
	 */
	protected $module;
	
	/**
	 *
	 * @var string
	 */
	protected $name;
	
	/**
	 * Method to set the value of field id
	 *
	 * @param integer $id        	
	 * @return $this
	 */
	public function setId($id) {
		$this->id = $id;
		
		return $this;
	}
	
	/**
	 * Method to set the value of field module
	 *
	 * @param string $module        	
	 * @return $this
	 */
	public function setModule($module) {
		$this->module = $module;
		
		return $this;
	}
	
	/**
	 * Method to set the value of field name
	 *
	 * @param string $name        	
	 * @return $this
	 */
	public function setName($name) {
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * Returns the value of field id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Returns the value of field module
	 *
	 * @return string
	 */
	public function getModule() {
		return $this->module;
	}
	
	/**
	 * Returns the value of field name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->hasMany ( 'id', 'Users', 'user_type', array (
				'alias' => 'Users' 
		) );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'user_types';
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return UserTypes[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return UserTypes
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
}
