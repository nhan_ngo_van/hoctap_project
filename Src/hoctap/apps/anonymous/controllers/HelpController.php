<?php

namespace hoctap\Anonymous\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\WebInfo;

/**
 *
 * @var string link to login
 */
const LinkLogin = '/anonymous/user/login';
const LinkIndex = '/index/index/';
const QuestionComment = "qus";
const LessonComment = "less";
const ExamComment = "exam";
class HelpController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->assets->addJs ( '/public/js/editer/basic/ckeditor.js' );
		$this->tag->prependTitle ( ' - Ký tự toán học - ' );
	}
	/**
	 * index page
	 */
	public function indexAction() {
	}
	
	/**
	 * View lesson in public user mode
	 */
	public function syntaxAction() {
		$this->view->content = WebInfo::findFirstByTag ( 'syntax' )->content;
	}
}

