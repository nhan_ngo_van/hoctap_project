<?php

namespace hoctap\Anonymous\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Exams;
use hoctap\Models\QuestionOfExam;
use hoctap\Models\Lessons;
use hoctap\Models\Questions;
use hoctap\Models\Answers;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\CommentsOfQuestions;
use hoctap\Models\CommentsOfLessons;
use hoctap\Models\CommentsOfExams;
use hoctap\Anonymous\Models\Users;
use hoctap\Models\Categories;
use hoctap\Models\Subjects;
use hoctap\Models\QuestionAnswer;
use hoctap\Models\AnswerTopic;

/**
 *
 * @var string link to login
 */
const LinkLogin = '/anonymous/user/login';
const LinkIndex = '/index/index/';
const QuestionComment = "qus";
const LessonComment = "less";
const ExamComment = "exam";
class ExamController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->assets->addJs ( '/public/js/editer/basic/ckeditor.js' );
		$this->tag->prependTitle ( ' - Đề thi - ' );
	}
	/**
	 * index page
	 */
	public function indexAction() {
		$subjects = Subjects::find ();
		$this->view->subjects = $subjects;
	}
	
	/**
	 * View exam
	 *
	 * @param integer $id        	
	 */
	public function examAction($id) {
		if (! $this->request->isPost ()) {
			
			$numberPage = $this->request->getQuery ( "page", "int" );
			$now = date ( 'Y-m-d H:i:s' );
			$exam = Exams::findFirstById ( $id );
			if ($exam && $exam->active_test_until < $now && $exam->active_view < $now) {
				$this->tag->prependTitle ( $exam->name );
				$this->view->exam = $exam;
				$questions = QuestionOfExam::find ( array (
						'conditions' => 'exam_id=?1',
						'bind' => array (
								1 => $exam->id 
						),
						'order' => 'question_number asc' 
				) );
				$this->view->questions = $questions;
				
				// load comment
				$comment = $this->getComments ( $id, ExamComment );
				$paginator = new Paginator ( array (
						'data' => $comment,
						'limit' => 15,
						'page' => $numberPage 
				) );
				$this->view->page = $paginator->getPaginate ();
				$this->view->viewComments = $this->initViewComment ( $id );
				$this->view->pageControl = $this->getControlPaginator ( '/exam/exam/' . $id . '/', $this->view->page );
				
				// load comment module
				if ($this->session->has ( 'id' ) && $this->session->has ( 'username' )) {
					$this->view->comment = $this->getCommentModule ( ExamComment, $id );
				} else {
					$this->view->comment = "";
				}
			} else {
				$this->flash->error ( 'Không tìm thấy đề thi' );
				$this->forward ( LinkIndex );
			}
		}
	}
	
	/**
	 * List exam
	 */
	public function examsAction($subject_id) {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		$now = date ( 'Y-m-d H:i:s' );
		
		// setup data for page
		$exams = Exams::find ( array (
				'conditions' => 'active_test_until <= ?1  and subject_id = ?2',
				'bind' => array (
						1 => $now,
						2 => $subject_id 
				) 
		) );
		if (count ( $exams ) == 0) {
			$this->flash->notice ( "Did not find any exams" );
		}
		$paginator = new Paginator ( array (
				'data' => $exams,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/exam/exams', $this->view->page );
	}
	public function getComments($id, $kind) {
		$commentModel = null;
		// get data
		switch ($kind) {
			case QuestionComment :
				$commentModel = CommentsOfQuestions::find ( array (
						'conditions' => 'question_id =?1',
						'bind' => array (
								1 => $id 
						) 
				) );
				$url += "question/";
				break;
			case LessonComment :
				$commentModel = CommentsOfLessons::find ( array (
						'conditions' => 'lesson_id =?1',
						'bind' => array (
								1 => $id 
						) 
				) );
				$url += "lesson/";
				break;
			case ExamComment :
				$commentModel = CommentsOfExams::find ( array (
						'conditions' => 'exam_id =?1',
						'bind' => array (
								1 => $id 
						) 
				) );
				$url += "exam/";
				break;
		}
		return $commentModel;
	}
	/**
	 * set data to panigater now only init html
	 */
	public function initViewComment($examID) {
		$viewComments = '';
		$viewComments .= '<div class="none-print col-md-12">';
		foreach ( $this->view->page->items as $comment ) {
			
			$user = Users::findFirstById ( $comment->author_id );
			$viewComments .= '<div class="col-md-2"><label>' . $user->first_name . ' </label></div> ';
			$viewComments .= '<div class="col-md-8">' . $comment->content . ' </div> ';
			if ($comment->author_id == $this->session->get ( "id" )) {
				$viewComments .= '<div class="col-md-1">' . '<a href="/exam/deletec/' . $examID . '/' . $comment->id . '">' . '<i class="fa fa-minus-circle" aria-hidden="true"></i></a>' . '</div>';
			}
			
			$viewComments .= '<div class="time-comment col-md-12">' . $comment->date_modify . '</div>';
		}
		// $viewComments .= '</div>';
		$viewComments .= '</div>';
		return $viewComments;
	}
	public function deletecAction($examID, $id) {
		$comment = CommentsOfExams::findFirstById ( $id );
		if ($comment && $comment->author_id == $this->session->get ( ('id') )) {
			$comment->delete ();
		}
		$this->response->redirect ( '/exam/exam/' . $examID );
	}
}

