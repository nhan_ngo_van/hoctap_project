<?php

namespace hoctap\Anonymous\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Categories;
use hoctap\Models\Questions;

class AjaxController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->view->disable ();
	}
	public function indexAction() {
	}
	/**
	 * Load category base on subject
	 *
	 * @param integer $subject_id        	
	 */
	public function categoryQAction($subject_id) {
		$categories = Categories::find ( array (
				'conditions' => 'subject_id=?1',
				'bind' => array (
						1 => $subject_id 
				) 
		) );
		
		$html = '<selecter id="question_type" name="question_type" class="form-control">';
		foreach ( $categories as $item ) {
			$html .= "<option value='$item->id'>$item->type</option>";
		}
		$html .= '</selecter>';
		echo $html;
	}
	/**
	 * Load category base on subject
	 *
	 * @param integer $subject_id        	
	 */
	public function categoryLAction($subject_id) {
		$categories = Categories::find ( array (
				'conditions' => 'subject_id=?1',
				'bind' => array (
						1 => $subject_id 
				) 
		) );
		
		$html = '<selecter id="category_id" name="category_id" class="form-control">';
		foreach ( $categories as $item ) {
			$html .= "<option value='$item->id'>$item->type</option>";
		}
		$html .= '</selecter>';
		echo $html;
	}
	public function questionsAction($user, $subject) {
		$questions = Questions::find ( array (
				'conditions' => 'author_id=?1 and subject_id=?2',
				'bind' => array (
						1 => $user,
						2 => $subject 
				) 
		) );
		
		$html = '<table  class="table table-striped">';
		$html .= '<thead><tr><th>Id</th><th>Tên</th><th>Nội dung</th></tr></thead>';
		foreach ( $questions as $question ) {
			$question->question_name = htmlentities ( $question->question_name, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
			$html .= '<tr>';
			$html .= '<td>' . $question->id . '</td>';
			$html .= '<td>' . $this->tag->linkTo ( '/teacher/question/view/' . $question->id, $question->question_name ) . '</td>';
			// $html .= '<td>' . Categories::findFirstById ( $question->question_type )->type . '</td>';
			$html .= '<td id="c' . $question->id . '">' . $question->question_content . '</td>';
			// $html .= '<td>' . Subjects::findFirstById ( $question->subject_id )->name . '</td>';
			$html .= '<td class="btn btn-default " onclick="addQ(' . $question->id . ')"><i class="glyphicon glyphicon-plus"></i> Thêm</td>';
			$html .= '</tr>';
		}
		
		$html . '</table';
		echo $html;
	}
	/**
	 * for function /teacher/exam/update/
	 *
	 * @param integer $id
	 *        	id of question
	 * @return string content of question
	 */
	public function questionAction($id) {
		$question = Questions::findFirstById ( $id );
		if ($question) {
			echo $question->question_content;
		} else {
			echo "[Err]Không có câu hỏi này";
		}
	}
}

