<?php

namespace hoctap\Anonymous\Controllers;

use hoctap\Base\Controllers\ControllerBase;

class ErrorsController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
	}
	public function show404Action() {
		$this->tag->setTitle ( 'Error 404' );
		$message = $this->request->get ( 'message' );
		$this->flash->error ( 'Error 404:' . $message );
	}
	public function show401Action() {
		$this->tag->setTitle ( 'Error 401' );
		$message = $this->request->get ( 'message' );
		$this->flash->error ( 'Error 401: ' . $message );
	}
	public function show500Action() {
		$this->tag->setTitle ( 'Error 500' );
		$message = $this->request->get ( 'message' );
		$this->flash->error ( 'Error 500: ' . $message );
	}
}

