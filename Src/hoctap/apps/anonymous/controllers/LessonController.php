<?php

namespace hoctap\Anonymous\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Lessons;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\CommentsOfQuestions;
use hoctap\Models\CommentsOfLessons;
use hoctap\Models\CommentsOfExams;
use hoctap\Anonymous\Models\Users;
use hoctap\Models\Categories;
use hoctap\Models\Subjects;
use hoctap\Models\QuestionAnswer;
use hoctap\Models\AnswerTopic;

/**
 *
 * @var string link to login
 */
const LinkLogin = '/anonymous/user/login';
const LinkIndex = '/index/index/';
const QuestionComment = "qus";
const LessonComment = "less";
const ExamComment = "exam";
class LessonController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->assets->addJs ( '/public/js/editer/basic/ckeditor.js' );
		$this->tag->prependTitle ( ' - Bài giảng - ' );
	}
	/**
	 * index page
	 */
	public function indexAction() {
		$subjects = Subjects::find ();
		$this->view->subjects = $subjects;
	}
	
	/**
	 * View lesson in public user mode
	 */
	public function lessonAction($id) {
		if (! $this->request->isPost ()) {
			
			$numberPage = $this->request->getQuery ( "page", "int" );
			
			$lesson = Lessons::findFirstById ( $id );
			if ($lesson) {
				$this->tag->prependTitle ( $lesson->name );
				$this->view->lesson = $lesson;
				
				// load comment
				$comment = $this->getComments ( $id, LessonComment );
				$paginator = new Paginator ( array (
						'data' => $comment,
						'limit' => 15,
						'page' => $numberPage 
				) );
				
				$this->view->page = $paginator->getPaginate ();
				$this->view->viewComments = $this->initViewComment ( $id );
				$this->view->pageControl = $this->getControlPaginator ( '/lesson/lesson/' . $id . '/', $this->view->page );
				
				// load comment module
				if ($this->session->has ( 'id' ) && $this->session->has ( 'username' )) {
					$this->view->comment = $this->getCommentModule ( LessonComment, $id );
				} else {
					$this->view->comment = "";
				}
			} else {
				$this->flash->error ( 'Không tìm thấy bài giảng' );
				$this->forward ( LinkIndex );
			}
		}
	}
	public function lessonsAction($subject) {
		// handle request for paginator
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$lesson = Lessons::find ( array (
				'conditions' => 'subject_id=?1',
				'bind' => array (
						1 => $subject 
				) 
		) );
		if (count ( $lesson ) == 0) {
			$this->flash->notice ( "Did not find any lesson" );
		}
		$paginator = new Paginator ( array (
				'data' => $lesson,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/lesson/lessons', $this->view->page );
	}
	public function getComments($id, $kind) {
		$commentModel = null;
		$url = '';
		// get data
		switch ($kind) {
			case QuestionComment :
				$commentModel = CommentsOfQuestions::find ( array (
						'conditions' => 'question_id =?1',
						'bind' => array (
								1 => $id 
						) 
				) );
				$url += "question/";
				break;
			case LessonComment :
				$commentModel = CommentsOfLessons::find ( array (
						'conditions' => 'lesson_id =?1',
						'bind' => array (
								1 => $id 
						) 
				) );
				$url += "lesson/";
				break;
			case ExamComment :
				$commentModel = CommentsOfExams::find ( array (
						'conditions' => 'exam_id =?1',
						'bind' => array (
								1 => $id 
						) 
				) );
				$url += "exam/";
				break;
		}
		return $commentModel;
	}
	/**
	 * set data to panigater now only init html
	 */
	public function initViewComment($lessonID) {
		$viewComments = '';
		$viewComments .= '<div class="none-print col-md-12">';
		foreach ( $this->view->page->items as $comment ) {
			
			$user = Users::findFirstById ( $comment->author_id );
			$viewComments .= '<div class="col-md-2"><label>' . $user->first_name . ' </label></div> ';
			$viewComments .= '<div class="col-md-8">' . $comment->content . ' </div> ';
			if ($comment->author_id == $this->session->get ( "id" )) {
				$viewComments .= '<div class="col-md-1">' . '<a href="/lesson/deletec/' . $lessonID . '/' . $comment->id . '">' . '<i class="fa fa-minus-circle" aria-hidden="true"></i></a>' . '</div>';
			}
			$viewComments .= '<div class="time-comment col-md-12">' . $comment->date_modify . '</div>';
		}
		// $viewComments .= '</div>';
		$viewComments .= '</div>';
		return $viewComments;
	}
	public function deletecAction($lessonID, $id) {
		$comment = CommentsOfLessons::findFirstById ( $id );
		if ($comment && $comment->author_id == $this->session->get ( ('id') )) {
			$comment->delete ();
		}
		$this->response->redirect ( '/lesson/lesson/' . $lessonID );
	}
}

