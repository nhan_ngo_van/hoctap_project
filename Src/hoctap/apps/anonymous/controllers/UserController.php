<?php

namespace hoctap\Anonymous\Controllers;

// require_once '/../libraries/Facebook/autoload.php';
use hoctap\Base\Controllers\ControllerBase;
use hoctap\Anonymous\Models\Users;
use Phalcon\Http\Response;
use hoctap\Anonymous\Models\UserTypes;
use hoctap\Forms\UserForm;
use Facebook;
use Phalcon\Forms\Element\Date;
use const hoctap\Base\Controllers\WEB_TITLE;
use const hoctap\Base\Controllers\WEB_ADDRESS;
use hoctap\Models\WebInfo;
use const hoctap\Base\Controllers\IS_STUDENT;
use const hoctap\Base\Controllers\IS_TEACHER;

// require_once APP_PATH . '/apps/libraries/Mailer/PHPMailerAutoLoad.php';
/**
 * Link to register action
 *
 * @var string
 */
const LinkRegister = '/user/register/';
const LinkFacebookRegister = '/user/facebooklogin/';
const FACEBOOK_APP_ID = '1820478491562817';
const FACEBOOK_APP_SEC = '0b227f435617d830ed6523c61a524ed2';
/**
 * Default module for user register only for student
 *
 * @var string
 */
const ModuleForRegister = 'student';
/**
 * Class for register new user/login to start session/ logout of session
 *
 * @author nhannv-pc
 *        
 */
class UserController extends ControllerBase {
	/**
	 * Init
	 *
	 * {@inheritdoc}
	 *
	 * @see \hoctap\Anonymous\Controllers\ControllerBase::initialize()
	 */
	public function initialize() {
		parent::initialize ();
		$this->assets->addCss ( '/public/css/a-user.css', true );
		$this->tag->setTitle ( 'Người dùng' );
	}
	/**
	 * Show information of current user
	 */
	public function indexAction() {
		// if (! $this->session->has ( 'facebook' )) {
		$this->view->name = $this->session->get ( 'name' );
		$this->view->username = $this->session->get ( 'username' );
		$this->view->id = $this->session->get ( 'id' );
		$user = Users::findFirstById ( $this->session->get ( 'id' ) );
		if ($user) {
			$this->view->user = $user;
		} else {
			$this->response->redirect ( '/' );
			$this->flash->error ( 'Có lỗi trong quá trình tìm' );
		}
		// } else {
		// // setup data for page
		// $found = Users::find ( array (
		// 'conditions' => 'user_name =?1',
		// 'bind' => array (
		// 1 => $this->session->get ( 'id' )
		// )
		// ) );
		// if ($found) {
		// $this->view->user = $found[0];
		// } else {
		// $this->response->redirect ( '/' );
		// $this->flash->error ( 'Có lỗi trong quá trình tìm' );
		// }
		// }
	}
	/**
	 * register new user
	 */
	public function registerAction() {
		$this->tag->setTitle ( 'Đăng ký' );
		if (! $this->session->has ( 'username' )) {
			
			if ($this->request->isPost ()) {
				// check this count have or not by username or email
				$found = Users::find ( array (
						'conditions' => 'user_name =?1 or email = ?2',
						'bind' => array (
								1 => $this->request->getPost ( 'user_name' ),
								2 => $this->request->getPost ( 'email' ) 
						) 
				) );
				if (count ( $found ) > 0) {
					$this->flash->error ( 'Tài khoản hoặc email đã được đăng ký' );
					return $this->response->redirect ( '/anonymous/user/login/' );
				}
				$newUser = new Users ();
				$newUser->user_name = $this->request->getPost ( 'user_name' );
				$newUser->first_name = $this->request->getPost ( 'first_name' );
				$newUser->last_name = $this->request->getPost ( 'last_name' );
				$newUser->bithday = $this->request->getPost ( 'birthday' );
				$newUser->email = $this->request->getPost ( 'email' );
				$newUser->join_date = date ( 'Y-m-d' );
				$newUser->education_level = 0;
				$passwordText = $this->request->getPost ( 'password' );
				$password = $this->getPassword ( $passwordText );
				$newUser->password = $password;
				$newUser->active = 0;
				
				// $newUser->user_type = $this->_getIdUserType ( ModuleForRegister );
				$requestType = $this->request->getPost ( "user_type", 'int' );
				
				if ($requestType == IS_STUDENT || $requestType == IS_TEACHER) {
					$newUser->user_type = $requestType;
					error_log ( "student/teacher" );
				} else {
					$newUser->user_type = IS_STUDENT;
				}
				if (! $newUser->save ()) {
					foreach ( $newUser->getMessages () as $message ) {
						$this->flash->error ( $message );
					}
					return $this->dispatcher->forward ( array (
							'module' => 'anonymous',
							"controller" => "user",
							"action" => "register" 
					) );
				} else {
					$this->_sendActiveEmail ( $newUser );
				}
				$this->flash->success ( 'Đăng ký thành công' );
				// redirect to other module cannot use forward
				$response = new Response ();
				return $response->redirect ( '/anonymous/user/login/' );
			}
		} else {
			$this->response->redirect ( $this->session->get ( 'module' ) . '/index/index/' );
		}
	}
	public function updateAction() {
		$this->tag->setTitle ( 'Cập nhật thông tin cá nhân' );
		$id = $this->session->get ( 'id' );
		if (! $this->request->isPost ()) {
			$user = Users::findFirstById ( $id );
			if (! $user) {
				$this->flash->error ( 'Không tìm thấy theo id' );
			}
			// validate owner
			if ($user->id == $this->session->get ( 'id' )) {
				$this->view->user = $user;
				$this->view->form = new UserForm ( $user, array (
						'edit' => true 
				) );
			} else {
				$this->flash->error ( 'Bạn không có quyền truy cập' );
			}
		}
	}
	/**
	 * Login to start session
	 */
	public function loginAction() {
		$this->tag->setTitle ( 'Đăng nhập' );
		$fb = new Facebook\Facebook ( [ 
				'app_id' => FACEBOOK_APP_ID,
				'app_secret' => '0b227f435617d830ed6523c61a524ed2',
				'default_graph_version' => 'v2.2' 
		] );
		$helper = $fb->getRedirectLoginHelper ();
		$permissons = [ 
				'email' 
		];
		$this->view->faceLogin = $helper->getLoginUrl ( WEB_ADDRESS . '/user/callback/', $permissons );
		if (! $this->session->has ( 'username' )) {
			// when request is post from action
			if ($this->request->isPost ()) {
				
				// get username and password
				$username = $this->request->getPost ( 'username' );
				$password = $this->getPassword ( $this->request->getPost ( 'password' ) );
				
				// search first user in database follow username and password
				$user = Users::findFirst ( array (
						"(user_name = :username: ) and password = :password:",
						'bind' => array (
								'username' => $username,
								'password' => $password 
						) 
				) );
				
				// start session
				if ($user != false && $user->active != 0) {
					$this->flash->success ( "Đăng nhập thành công!" );
					$userType = $this->_getUserType ( $user->user_type );
					$this->_registerSession ( $user, $userType->getModule () );
					
					// redirect to module
					$this->response->redirect ( $userType->getModule () . '/index/index/' );
				} else {
					// alert invalid username and password and login again
					$this->flash->error ( "Tài khoản hoặc mật khẩu không hợp lệ. Vui lòng kích hoạt tài khoản trước" );
				}
			}
		} else {
			// redirect to module
			$this->response->redirect ( $this->session->get ( 'module' ) . '/index/index/' );
		}
	}
	/**
	 * Login with facebook instead of create new acount
	 */
	public function callbackAction() {
		$fb = new Facebook\Facebook ( [ 
				'app_id' => FACEBOOK_APP_ID, // Replace {app-id} with your app id
				'app_secret' => FACEBOOK_APP_SEC,
				'default_graph_version' => 'v2.2' 
		] );
		
		$helper = $fb->getRedirectLoginHelper ();
		
		try {
			$accessToken = $helper->getAccessToken ();
		} catch ( Facebook\Exceptions\FacebookResponseException $e ) {
			// When Graph returns an error
			echo 'Graph returned an error: ' . $e->getMessage ();
			exit ();
		} catch ( Facebook\Exceptions\FacebookSDKException $e ) {
			// When validation fails or other local issues
			echo 'Facebook SDK returned an error: ' . $e->getMessage ();
			exit ();
		}
		
		if (! isset ( $accessToken )) {
			if ($helper->getError ()) {
				header ( 'HTTP/1.0 401 Unauthorized' );
				echo "Error: " . $helper->getError () . "\n";
				echo "Error Code: " . $helper->getErrorCode () . "\n";
				echo "Error Reason: " . $helper->getErrorReason () . "\n";
				echo "Error Description: " . $helper->getErrorDescription () . "\n";
			} else {
				header ( 'HTTP/1.0 400 Bad Request' );
				echo 'Bad request';
			}
			exit ();
		}
		
		// Logged in
		// echo '<h3>Access Token</h3>';
		// var_dump ( $accessToken->getValue () );
		
		// The OAuth 2.0 client handler helps us manage access tokens
		$oAuth2Client = $fb->getOAuth2Client ();
		
		// Get the access token metadata from /debug_token
		$tokenMetadata = $oAuth2Client->debugToken ( $accessToken );
		// echo '<h3>Metadata</h3>';
		// var_dump ( $tokenMetadata );
		
		// Validation (these will throw FacebookSDKException's when they fail)
		$tokenMetadata->validateAppId ( FACEBOOK_APP_ID );
		// If you know the user ID this access token belongs to, you can validate it here
		// $tokenMetadata->validateUserId('123');
		$tokenMetadata->validateExpiration ();
		
		if (! $accessToken->isLongLived ()) {
			// Exchanges a short-lived access token for a long-lived one
			try {
				$accessToken = $oAuth2Client->getLongLivedAccessToken ( $accessToken );
			} catch ( Facebook\Exceptions\FacebookSDKException $e ) {
				echo "<p>Error getting long-lived access token: " . $helper->getMessage () . "</p>\n\n";
				exit ();
			}
			
			echo '<h3>Long-lived</h3>';
			var_dump ( $accessToken->getValue () );
		}
		try {
			// Returns a `Facebook\FacebookResponse` object
			$response = $fb->get ( '/me?fields=name,email', $accessToken );
		} catch ( Facebook\Exceptions\FacebookResponseException $e ) {
			echo 'Graph returned an error: ' . $e->getMessage ();
			exit ();
		} catch ( Facebook\Exceptions\FacebookSDKException $e ) {
			echo 'Facebook SDK returned an error: ' . $e->getMessage ();
			exit ();
		}
		
		$facebookUser = $response->getGraphUser ();
		if ($facebookUser ['email'] == '') {
			$this->flash->error ( 'Không thể truy cập email để tạo mật khẩu' );
			$this->response->redirect ( '/user/login' );
			return;
		}
		$this->session->set ( 'name', $facebookUser ['name'] );
		$this->session->set ( 'module', 'student' );
		
		$this->session->set ( 'username', $facebookUser ['email'] );
		$this->session->set ( 'facebook', $facebookUser->getId () );
		$this->session->set ( 'icon', 'https://graph.facebook.com/' . $facebookUser->getId () . '/picture?width=50&height=50' );
		// $this->flash->success ( $facebookUser ['email'] );
		// setup data for page
		$found = Users::find ( array (
				'conditions' => 'user_name =?1',
				'bind' => array (
						1 => $facebookUser ['email'] 
				) 
		) );
		if (! count ( $found ) > 0) {
			$user = new Users ();
			$user->facebook = $facebookUser->getId ();
			$user->user_name = $facebookUser ['email'];
			$user->email = $facebookUser ['email'];
			$user->join_date = date ( 'Y-m-d' );
			$user->first_name = $facebookUser->getName ();
			$user->user_type = $this->_getIdUserType ( ModuleForRegister );
			$user->password = $this->getPassword ();
			$user->active = 0; // cannot access by account, only after change password
			$user->education_level = 0;
			if ($user->save ()) {
				$this->flash->success ( 'Tạo tài khoản thành công' );
				$this->session->set ( 'id', $user->id );
			} else {
				$this->flash->error ( 'Tạo tài khoản không thành công, vui lòng thử lại sau' );
				foreach ( $user->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
			}
		} else {
			$this->session->set ( 'id', $found [0]->id );
		}
		$this->response->redirect ( '/anonymous/index/index/' );
	}
	/**
	 * Change password action
	 */
	public function passwordAction() {
		if ($this->request->isPost ()) {
			$currentPassword = $this->request->getPost ( 'old-password' );
			
			$user = Users::findFirst ( array (
					"(user_name = :username: ) and password = :password:",
					'bind' => array (
							'username' => $this->session->get ( 'username' ),
							'password' => $this->getPassword ( $currentPassword ) 
					) 
			) );
			if ($user) {
				$newPass = $this->request->getPost ( 'new-password' );
				$confirmPass = $this->request->getPost ( 'confirm-password' );
				if ($confirmPass == $newPass) {
					$user->password = $this->getPassword ( $newPass );
					$user->active = 1; // for case facebook user change password, only can access after change password
					if ($user->save ()) {
						$this->session->destroy ();
						$this->flash->success ( 'Cập nhật mật khẩu thành công' );
						$this->response->redirect ( '/anonymous/user/login' );
					} else {
						$this->flash->error ( 'Xảy ra lỗi, vui lòng thử lại sau' );
					}
				} else {
					$this->flash->error ( 'Mật khẩu mới không khớp' );
				}
			} else {
				$this->flash->error ( 'Mật khẩu hiện tại không đúng' );
			}
		}
	}
	/**
	 * Logout action to exit current user
	 */
	public function logoutAction() {
		$this->session->destroy ();
		$this->response->redirect ( '/anonymous/index/index/' );
	}
	/**
	 * Reset password when user is forgot
	 */
	public function forgotAction() {
		if ($this->request->isPost ()) {
			$toEmail = $this->request->getPost ( 'email' );
			$user = Users::findFirst ( array (
					"(email = :email: )",
					'bind' => array (
							'email' => $toEmail 
					) 
			) );
			if (! $user) {
				$this->flash->error ( 'Email không tồn tại' );
				$this->response->redirect ( '/' );
			} else {
				$newPassword = $this->generatePassword ( 8 );
				$user->password = $this->getPassword ( $newPassword );
				
				if ($user->save ()) {
					
					$content = 'Mật khẩu mới của bạn là:<br/>' . $newPassword;
					$this->sendEmail ( MAIL_ADDRESS, $toEmail, WEB_TITLE . ': Đổi mật khẩu', $content );
					$this->response->redirect ( '/user/login/' );
				} else {
					$this->flash->error ( 'Xảy ra lỗi trong quá trình gửi, vui lòng thử lại sau<br/>' );
				}
			}
		}
	}
	/**
	 * Update user profile
	 *
	 * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
	 */
	public function saveAction() {
		if ($this->request->isPost ()) {
			$id = $this->request->getPost ( 'id', 'int' );
			$user = Users::findFirstById ( $id );
			if ($user && $id == $this->session->get ( 'id' )) {
				$form = new UserForm ();
				$this->view->form = $form;
				$data = $this->request->getPost ();
				if ($form->isValid ( $data, $user )) {
					if ($user->save () == false) {
						foreach ( $user->getMessages () as $message ) {
							$this->flash->error ( $message );
						}
					}
					return $this->response->redirect ( '/anonymous/user/index/' );
					$this->flash->success ( 'Cập nhật thành công' );
				} else {
					$this->flash->error ( 'Dữ liệu không hợp lệ' );
				}
			}
		}
	}
	public function activeAction($email, $password) {
		$user = Users::find ( array (
				"password = :password:",
				'bind' => array (
						'password' => $password 
				) 
		) );
		if (count ( $user ) > 0) {
			for($i = 0; $i < count ( $user ); $i ++) {
				if ($this->getPassword ( $user [$i]->email ) == $email) {
					$user [$i]->active = 1;
					$user [$i]->save ();
					$this->flash->success ( "Kích hoạt thành công" );
					break;
				}
			}
		}
		$this->response->redirect ( '/user/login/' );
	}
	/**
	 *
	 * @param Users $user        	
	 */
	private function _registerSession(Users $user, $module) {
		$this->session->set ( 'id', $user->id );
		$this->session->set ( 'username', $user->user_name );
		$this->session->set ( 'name', $user->first_name );
		$this->session->set ( 'module', $module );
		$this->session->set ( 'icon', '/public/files/user-icon/' . ($user->education_level < 10 ? '0' : '') . $user->education_level . '.png' );
	}
	/**
	 * get user type of current user
	 *
	 * @param int $idType        	
	 * @return UserTypes
	 */
	private function _getUserType($idType) {
		$userType = UserTypes::findFirstById ( $idType );
		return $userType;
	}
	/**
	 * name of module
	 *
	 * @param string $module        	
	 */
	private function _getIdUserType($module) {
		$userType = UserTypes::findFirst ( array (
				'module' => $module 
		) );
		if ($userType != false) {
			return $userType->getId ();
		} else {
			return false;
		}
	}
	private function generatePassword($length = 8) {
		$chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+';
		$count = mb_strlen ( $chars );
		
		for($i = 0, $result = ''; $i < $length; $i ++) {
			$index = rand ( 0, $count - 1 );
			$result .= mb_substr ( $chars, $index, 1 );
		}
		
		return $result;
	}
	private function _sendActiveEmail($newUser) {
		$title = WebInfo::findFirstByTag ( 'first_mail' );
		$end = WebInfo::findFirstByTag ( 'last_mail' );
		$content = $title->content;
		$content .= '<a href="' . WEB_ADDRESS . '/user/active/' . $this->getPassword ( $newUser->email ) . '/' . $newUser->password . '">Here</a>';
		$content .= $end->content;
		// send email to active account after register
		$this->sendEmail ( MAIL_ADDRESS, $newUser->email, WEB_TITLE . ': Active acount', $content );
	}
}

