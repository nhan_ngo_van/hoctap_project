<?php

namespace hoctap\Anonymous\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Exams;
use hoctap\Models\QuestionOfExam;
use hoctap\Models\Lessons;
use hoctap\Models\Questions;
use hoctap\Models\Answers;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\CommentsOfQuestions;
use hoctap\Models\CommentsOfLessons;
use hoctap\Models\CommentsOfExams;
use hoctap\Anonymous\Models\Users;
use hoctap\Models\Categories;
use hoctap\Models\Subjects;
use hoctap\Models\QuestionAnswer;
use hoctap\Models\AnswerTopic;
use hoctap\Models\ResultTest;
use hoctap\Models\ChartData;

/**
 *
 * @var string link to login
 */
const LinkLogin = '/anonymous/user/login';
const LinkIndex = '/index/index/';
const QuestionComment = "qus";
const LessonComment = "less";
const ExamComment = "exam";
class TestController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->assets->addJs ( '/public/js/anonymous/main.js' );
	}
	/**
	 * index page
	 */
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		$now = date ( 'Y-m-d H:i:s' );
		// setup data for page
		$exams = Exams::find ( array (
				'conditions' => ' active_view < ?1 and active_test_until > ?1 ',
				'bind' => array (
						1 => $now 
				) 
		) );
		if (count ( $exams ) == 0) {
			$this->flash->notice ( "Did not find any exams" );
		}
		$paginator = new Paginator ( array (
				'data' => $exams,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/test/exams', $this->view->page );
	}
	
	/**
	 * View exam
	 *
	 * @param integer $id        	
	 */
	public function examAction($id) {
		if (! $this->request->isPost ()) {
			$exam = Exams::findFirstById ( $id );
			$now = date ( 'Y-m-d H:i:s' );
			if ($exam && $exam->active_test_until > $now && $exam->active_view < $now) {
				// check user tested or not if was test -> not for test again
				$tested = ResultTest::find ( array (
						'conditions' => 'exam_id = ?1 and user_id = ?2 ',
						'bind' => array (
								1 => $exam->id,
								2 => $this->session->get ( 'id' ) 
						) 
				) );
				// started test
				if (count ( $tested ) > 0) {
					// case tested but want to check and update and still have time
					if (strtotime ( $tested [0]->date_test ) + $exam->limit_time * 60 - strtotime ( $now ) > 0 && $tested [0]->choose_answer == "") {
						$this->session->set ( 'teststart', $tested [0]->date_test );
					} else {
						// tested and have not time
						$this->flash->error ( 'Bạn đã thực hiện bài thi [' . $exam->name . ']' );
						return $this->response->redirect ( '/test/' );
					}
					// case start new test
				} else {
					$this->session->set ( 'teststart', $now );
					$testResult = new ResultTest ();
					$testResult->date_test = $now;
					$testResult->exam_id = $exam->id;
					$testResult->user_id = $this->session->get ( 'id' );
					if (! $testResult->save ()) {
						$this->flash->error ( 'Error' );
					}
				}
				
				$now = strtotime ( date ( 'Y-m-d H:i:s' ) );
				$start = strtotime ( date ( $this->session->get ( 'teststart' ) ) );
				
				$this->view->remainTime = $start + $exam->limit_time * 60 - $now;
				
				$this->tag->setTitle ( $exam->name );
				$this->view->exam = $exam;
				$questions = QuestionOfExam::find ( array (
						'conditions' => 'exam_id=?1',
						'bind' => array (
								1 => $exam->id 
						),
						'order' => 'question_number asc' 
				) );
				$this->view->questions = $questions;
			} else {
				$this->flash->error ( 'Không tìm thấy đề thi' );
				return $this->response->redirect ( '/test/' );
			}
		}
	}
	/**
	 * confirm to go to test
	 */
	public function confirmAction() {
	}
	/**
	 * finish testing and save result
	 */
	public function finishAction() {
		$size = $this->request->getPost ( 'number_question' );
		$id = $this->request->getPost ( 'exam_id' );
		$chooseAnswers = '';
		$totalScore = 0;
		$yourScore = 0;
		$questions = QuestionOfExam::find ( array (
				'conditions' => 'exam_id=?1',
				'bind' => array (
						1 => $id 
				),
				'order' => 'question_number asc' 
		) );
		for($i = 1; $i <= $size; $i ++) {
			
			$chartData = new ChartData ();
			$chartData->category_id = $questions [$i - 1]->question_type;
			$chartData->exam_id = $id;
			$chartData->user_id = $this->session->get ( 'id' );
			$chartData->subject_id = 0;
			
			$chooseAnswers .= $i . '_';
			$chooseAnswers .= $questions [$i - 1]->question_id . '_';
			$chooseAnswers .= $this->request->getPost ( 'q' . $i ) . '_';
			$chooseAnswers .= $questions [$i - 1]->true_answer;
			$chooseAnswers .= ';';
			if ($this->request->getPost ( 'q' . $i ) == $questions [$i - 1]->true_answer) {
				$chartData->choose_correct = 1;
				$yourScore += $questions [$i - 1]->score;
			} else {
				$chartData->choose_correct = 0;
			}
			
			$chartData->save ();
			$totalScore += $questions [$i - 1]->score;
		}
		$chooseAnswerModel = ResultTest::find ( array (
				'conditions' => 'exam_id = ?1 and user_id = ?2 ',
				'bind' => array (
						1 => $id,
						2 => $this->session->get ( 'id' ) 
				) 
		) );
		if (count ( $chooseAnswerModel )) {
			// $chooseAnswerModel->exam_id = $id;
			$chooseAnswerModel [0]->choose_answer = $chooseAnswers;
			// $chooseAnswerModel->user_id = $this->session->get ( 'id' );
			$chooseAnswerModel [0]->score = $yourScore . ' / ' . $totalScore;
			if (! $chooseAnswerModel [0]->save ()) {
				$this->flash->error ( 'Chưa nộp bài thành công, quay lại để nộp' );
			} else {
				$this->flash->success ( 'Nộp bài thành công' );
				$this->view->yourScore = $yourScore;
				$this->view->totalScore = $totalScore;
			}
		}
		$this->session->remove ( 'teststart' );
	}
}

