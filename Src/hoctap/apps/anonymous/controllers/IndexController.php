<?php

namespace hoctap\Anonymous\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Exams;
use hoctap\Models\QuestionOfExam;
use hoctap\Models\Lessons;
use hoctap\Models\Questions;
use hoctap\Models\Answers;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\CommentsOfQuestions;
use hoctap\Models\CommentsOfLessons;
use hoctap\Models\CommentsOfExams;
use hoctap\Anonymous\Models\Users;
use hoctap\Models\Categories;
use hoctap\Models\Subjects;
use hoctap\Models\QuestionAnswer;
use hoctap\Models\AnswerTopic;

/**
 *
 * @var string link to login
 */
const LinkLogin = '/anonymous/user/login';
const LinkIndex = '/index/index/';
const QuestionComment = "qus";
const LessonComment = "less";
const ExamComment = "exam";
class IndexController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->assets->addJs ( '/public/js/editer/basic/ckeditor.js' );
	}
	/**
	 * index page
	 */
	public function indexAction() {
		$limitView = 10;
		$this->view->newestLessons = $this->_newLessons ( $limitView );
		$this->view->newestQuestions = $this->_newQuestions ( $limitView );
		$this->view->newestExams = $this->_newExams ( $limitView );
	}
	/**
	 * View question in public mode
	 */
	public function questionAction($id) {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			
			// page number
			$numberPage = $this->request->getQuery ( "page", "int" );
			
			// get question
			$question = Questions::findFirstById ( $id );
			if ($question) {
				$this->tag->setTitle ( $question->question_name );
				$this->view->question = $question;
				$answers = Answers::find ( array (
						'conditions' => 'question_id=?1',
						'bind' => array (
								1 => $question->id 
						) 
				) );
				// show answer when current time less or equal active time
				if (count ( $answers ) > 0 && $answers [0]->active_view <= date ( 'Y-m-d H:i:s' )) {
					$this->view->answers = $answers;
				} else {
					$this->view->answers = null;
				}
				
				// load comment
				$comment = $this->getComments ( $id, QuestionComment );
				$paginator = new Paginator ( array (
						'data' => $comment,
						'limit' => 15,
						'page' => $numberPage 
				) );
				$this->view->page = $paginator->getPaginate ();
				$this->view->viewComments = $this->initViewComment ();
				$this->view->pageControl = $this->getControlPaginator ( '/index/question/' . $id . '/', $this->view->page );
				
				// load comment module
				if ($this->session->has ( 'id' ) && $this->session->has ( 'username' )) {
					$this->view->comment = $this->getCommentModule ( QuestionComment, $id );
				} else {
					$this->view->comment = "";
				}
			} else {
				$this->flash->error ( 'Không tìm thấy câu hỏi' );
				$this->forward ( LinkIndex );
			}
		}
	}
	/**
	 * View exam
	 *
	 * @param integer $id        	
	 */
	public function examAction($id) {
		if (! $this->request->isPost ()) {
			
			$numberPage = $this->request->getQuery ( "page", "int" );
			$now = date ( 'Y-m-d H:i:s' );
			$exam = Exams::findFirstById ( $id );
			if ($exam && $exam->active_test_until < $now && $exam->active_view < $now) {
				$this->tag->setTitle ( $exam->name );
				$this->view->exam = $exam;
				$questions = QuestionOfExam::find ( array (
						'conditions' => 'exam_id=?1',
						'bind' => array (
								1 => $exam->id 
						),
						'order' => 'question_number asc' 
				) );
				$this->view->questions = $questions;
				
				// load comment
				$comment = $this->getComments ( $id, ExamComment );
				$paginator = new Paginator ( array (
						'data' => $comment,
						'limit' => 15,
						'page' => $numberPage 
				) );
				$this->view->page = $paginator->getPaginate ();
				$this->view->viewComments = $this->initViewComment ();
				$this->view->pageControl = $this->getControlPaginator ( '/index/exam/' . $id . '/', $this->view->page );
				
				// load comment module
				if ($this->session->has ( 'id' ) && $this->session->has ( 'username' )) {
					$this->view->comment = $this->getCommentModule ( ExamComment, $id );
				} else {
					$this->view->comment = "";
				}
			} else {
				$this->flash->error ( 'Không tìm thấy đề thi' );
				$this->forward ( LinkIndex );
			}
		}
	}
	/**
	 * View lesson in public user mode
	 */
	public function lessonAction($id) {
		if (! $this->request->isPost ()) {
			
			$numberPage = $this->request->getQuery ( "page", "int" );
			
			$lesson = Lessons::findFirstById ( $id );
			if ($lesson) {
				$this->view->lesson = $lesson;
				
				// load comment
				$comment = $this->getComments ( $id, LessonComment );
				$paginator = new Paginator ( array (
						'data' => $comment,
						'limit' => 15,
						'page' => $numberPage 
				) );
				$this->view->page = $paginator->getPaginate ();
				$this->view->viewComments = $this->initViewComment ();
				$this->view->pageControl = $this->getControlPaginator ( '/index/lesson/' . $id . '/', $this->view->page );
				
				// load comment module
				if ($this->session->has ( 'id' ) && $this->session->has ( 'username' )) {
					$this->view->comment = $this->getCommentModule ( LessonComment, $id );
				} else {
					$this->view->comment = "";
				}
			} else {
				$this->flash->error ( 'Không tìm thấy bài giảng' );
				$this->forward ( LinkIndex );
			}
		}
	}
	public function lessonsAction() {
		// handle request for paginator
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$lesson = Lessons::find ();
		if (count ( $lesson ) == 0) {
			$this->flash->notice ( "Did not find any lesson" );
		}
		$paginator = new Paginator ( array (
				'data' => $lesson,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/index/lessons', $this->view->page );
	}
	/**
	 * List exam
	 */
	public function examsAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		$now = date ( 'Y-m-d H:i:s' );
		// setup data for page
		$exams = Exams::find ( array (
				'conditions' => 'active_test_until <= ?1 ',
				'bind' => array (
						1 => $now 
				) 
		) );
		if (count ( $exams ) == 0) {
			$this->flash->notice ( "Did not find any exams" );
		}
		$paginator = new Paginator ( array (
				'data' => $exams,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/index/exams', $this->view->page );
	}
	public function questionsAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$questions = Questions::find ();
		if (count ( $questions ) == 0) {
			$this->flash->notice ( "Did not find any questions" );
		}
		$paginator = new Paginator ( array (
				'data' => $questions,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/index/questions', $this->view->page );
	}
	/**
	 * Display list answer of a exam
	 *
	 * @param integer $examID        	
	 */
	public function answersAction($examID) {
		if (! $this->request->isPost ()) {
			$exam = Exams::findFirstById ( $examID );
			$now = date ( 'Y-m-d H:i:s' );
			if ($exam && $exam->active_answer_view < $now) {
				$this->tag->setTitle ( $exam->name );
				$this->view->exam = $exam;
				$questionAnswer = QuestionAnswer::find ( array (
						'conditions' => 'exam_id=?1',
						'bind' => array (
								1 => $exam->id 
						),
						'order' => 'question_number asc' 
				) );
				
				if ($exam->active_answer_view <= date ( 'Y-m-d H:i:s' )) {
					$this->view->questionAnswer = $questionAnswer;
				} else {
					$this->view->questionAnswer = null;
				}
			} else {
				$this->flash->error ( 'Không tìm thấy đáp án thời điệm hiện tại' );
				$this->response->redirect ( '/exam/exam/' . $examID );
			}
		}
	}
	/**
	 * Insert new comment
	 *
	 * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
	 */
	public function commentAction() {
		$this->view->disable ();
		$response = new \Phalcon\Http\Response ();
		if ($this->request->has ( "content" ) && $this->request->has ( "id" ) && $this->request->has ( "kind" )) {
			$comment = $this->request->get ( "content" );
			// size of comment is to log
			if (strlen ( $comment ) > 1000) {
				return;
			}
			$kind = $this->request->get ( "kind" );
			$id = $this->request->get ( "id" );
			$commentModel = null;
			$url = "/index/";
			switch ($kind) {
				case QuestionComment :
					$commentModel = new CommentsOfQuestions ();
					$commentModel->question_id = $id;
					$url += "question/";
					break;
				case LessonComment :
					$commentModel = new CommentsOfLessons ();
					$commentModel->lesson_id = $id;
					$url += "lesson/";
					break;
				case ExamComment :
					$commentModel = new CommentsOfExams ();
					$commentModel->exam_id = $id;
					$url += "exam/";
					break;
				case 'topic' :
					$commentModel = new AnswerTopic ();
					$commentModel->topic_id = $id;
					$url = '/forum/topic/';
					break;
			}
			if ($commentModel != null) {
				// insert <p>tag for disable script
				$commentModel->content = $comment;
				$commentModel->author_id = $this->session->get ( "id" );
				$commentModel->date_modify = date ( 'Y-m-d H:i:s' );
				
				$invalidChar = parent::containInvalidChar ( $comment );
				
				if ($invalidChar) {
					$this->flash->error ( "Nội dung comment chứa ký tự không được phép: " . $invalidChar );
				} else if (! $commentModel->save ()) {
					
					$this->flash->error ( "Xảy ra lỗi khi comment" );
					foreach ( $commentModel->getMessages () as $message ) {
						$this->flash->error ( $message );
					}
					$this->flash->error ( "id" + $id );
				}
				return $this->forward ( $url + $id );
			}
		}
	}
	
	/**
	 * newest lesson
	 */
	private function _newLessons($limit) {
		$newestLessons = Lessons::find ( array (
				'order' => 'created_date desc',
				'limit' => $limit 
		) );
		
		$viewLessons = '<table class="table table-striped"><tbody>';
		foreach ( $newestLessons as $lesson ) {
			$lesson->name = htmlentities ( $lesson->name, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
			$viewLessons .= '<tr>';
			$viewLessons .= '<td class="col-md-8">' . $this->tag->linkTo ( '/lesson/lesson/' . $lesson->id, $lesson->name ) . '</td>';
			$viewLessons .= '<td>' . Categories::findFirstById ( $lesson->category_id )->type . '</td>';
			$viewLessons .= '<td>' . Subjects::findFirstById ( $lesson->subject_id )->name . '</td>';
			$viewLessons .= '<td>' . Users::findFirstById ( $lesson->author_id )->first_name . '</td>';
			$viewLessons .= '</tr>';
		}
		$viewLessons .= '</tbody></table>';
		return $viewLessons;
	}
	/**
	 * newest question
	 */
	private function _newQuestions($limit) {
		$newestQuestion = Questions::find ( array (
				'order' => 'created_date desc',
				'limit' => $limit 
		) );
		
		$viewQuestions = '<table class="table table-striped"><tbody>';
		
		foreach ( $newestQuestion as $question ) {
			$question->question_name = htmlentities ( $question->question_name, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
			$viewQuestions .= '<tr>';
			$viewQuestions .= '<td class="col-md-8">' . $this->tag->linkTo ( '/index/question/' . $question->id, $question->question_name ) . '</td>';
			$viewQuestions .= '<td>' . Categories::findFirstById ( $question->question_type )->type . '</td>';
			$viewQuestions .= '<td>' . Subjects::findFirstById ( $question->subject_id )->name . '</td>';
			$viewQuestions .= '<td>' . Users::findFirstById ( $question->author_id )->first_name . '</td>';
			$viewQuestions .= '</tr>';
		}
		
		$viewQuestions .= '</tbody></table>';
		return $viewQuestions;
	}
	/**
	 * newest exam
	 */
	private function _newExams() {
		$limit = 10;
		$newestExams = Exams::find ( array (
				'order' => 'created_date desc',
				'limit' => $limit 
		) );
		
		$viewExam = '<table class="table table-striped"><tbody>';
		
		foreach ( $newestExams as $exam ) {
			$exam->name = htmlentities ( $exam->name, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
			$viewExam .= '<tr>';
			$viewExam .= '<td class="col-md-8">' . $this->tag->linkTo ( '/exam/exam/' . $exam->id, $exam->name ) . '</td>';
			// $viewExam .= '<td>' . Subjects::findFirstById ( $exam->subject_id )->name . '</td>';
			$viewExam .= '<td>' . Users::findFirstById ( $exam->author_id )->first_name . '</td>';
			$viewExam .= '</tr>';
		}
		
		$viewExam .= '</tbody></table>';
		
		return $viewExam;
	}
	public function getComments($id, $kind) {
		$commentModel = null;
		// get data
		switch ($kind) {
			case QuestionComment :
				$commentModel = CommentsOfQuestions::find ( array (
						'conditions' => 'question_id =?1',
						'bind' => array (
								1 => $id 
						) 
				) );
				$url += "question/";
				break;
			case LessonComment :
				$commentModel = CommentsOfLessons::find ( array (
						'conditions' => 'lesson_id =?1',
						'bind' => array (
								1 => $id 
						) 
				) );
				$url += "lesson/";
				break;
			case ExamComment :
				$commentModel = CommentsOfExams::find ( array (
						'conditions' => 'exam_id =?1',
						'bind' => array (
								1 => $id 
						) 
				) );
				$url += "exam/";
				break;
		}
		return $commentModel;
	}
	/**
	 * set data to panigater now only init html
	 */
	public function initViewComment() {
		$viewComments = '';
		foreach ( $this->view->page->items as $comment ) {
			$viewComments .= '<div class="none-print">';
			$user = Users::findFirstById ( $comment->author_id );
			$viewComments .= '<label>' . $user->first_name . ':</label> ';
			$viewComments .= $comment->content;
			$viewComments .= '<br/><div class="time-comment">' . $comment->date_modify . '</div>';
			$viewComments .= '</div>';
		}
		return $viewComments;
	}
}

