<?php

namespace hoctap\Anonymous\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Topics;
use hoctap\Forms\TopicForm;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\AnswerTopic;
use hoctap\Models\ViewTopicAnswer;

const LIMIT_VIEW = 15;
class ForumController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->tag->prependTitle ( ' - Forum - ' );
		$this->assets->addJs ( '/public/js/editer/basic/ckeditor.js' );
	}
	/**
	 * list up all topic
	 */
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		$topics = Topics::find ( array (
				'order' => 'create_date desc' 
		) );
		// 'limit' => LIMIT_VIEW
		
		$this->view->topics = $topics;
		$paginator = new Paginator ( array (
				'data' => $topics,
				'limit' => 20,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/forum/index', $this->view->page );
	}
	/**
	 * display a topic
	 *
	 * @param integer $id:
	 *        	topic id
	 */
	public function topicAction($id) {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
			
			$topic = Topics::findFirstById ( $id );
			if ($topic) {
				
				$this->tag->prependTitle ( $topic->name );
				$this->view->topic = $topic;
			} else {
				$this->flash->error ( 'Không tìm thấy topic' );
			}
			$answers = ViewTopicAnswer::find ( array (
					'conditions' => 'topic_id=?1',
					'bind' => array (
							1 => $id 
					) 
			) );
			$paginator = new Paginator ( array (
					'data' => $answers,
					'limit' => 15,
					'page' => $numberPage 
			) );
			$this->view->page = $paginator->getPaginate ();
			$this->view->pageControl = $this->getControlPaginator ( '/forum/topic/' . $id . '/', $this->view->page );
		}
		if ($this->session->has ( 'id' ) && $this->session->has ( 'username' )) {
			$this->view->comment = $this->getCommentModule ( 'topic', $id );
		} else {
			$this->view->comment = "";
		}
	}
	
	/**
	 * create new topic
	 */
	public function createAction() {
		if ($this->session->has ( 'id' ) && $this->session->has ( 'username' )) {
			$topicForm = new TopicForm ();
			if ($this->request->isPost ()) {
				
				$topic = new Topics ();
				$data = $this->request->getPost ();
				
				if ($topicForm->isValid ( $data, $topic )) {
					$topic->author_id = $this->session->get ( 'id' );
					$topic->create_date = date ( 'Y-m-d H:i:s' );
					$topic->date_modify = date ( 'Y-m-d H:i:s' );
					if (! $topic->save ()) {
						$this->flash->error ( 'Tạo topic không thành công' );
					} else {
						$this->flash->success ( 'Tạo topic thành công' );
						$this->response->redirect ( '/forum/topic/' . $topic->id );
					}
				} else {
					$this->flash->error ( 'Tạo topic không thành công' );
				}
			}
			$this->view->form = $topicForm;
		} else {
			$this->flash->error ( 'Bạn phải đăng nhập mới có quyền tạo topic' );
			$this->response->redirect ( '/user/login/' );
		}
	}
	/**
	 * answer a topic
	 *
	 * @param integer $id:
	 *        	topic id
	 */
	public function answerAction($id) {
	}
	/**
	 * delete an answer
	 *
	 * @param integer $id:
	 *        	id of answer_of_topic
	 */
	public function deleteAnswerAction($id) {
		$comment = AnswerTopic::findFirstById ( $id );
		if ($comment && $comment->author_id == $this->session->get ( ('id') )) {
			$comment->delete ();
		}
		$this->response->redirect ( '/forum/topic/' . $id );
	}
	/**
	 * update an answer
	 *
	 * @param integer $id:
	 *        	id of answer_topic
	 */
	public function updateAnswerAction($id) {
	}
}