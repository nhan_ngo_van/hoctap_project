<?php

namespace hoctap\Plugins;

use Phalcon\Mvc\User\Plugin;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;

const Default_Module = 'anonymous';
class SecurityPlugin extends Plugin {
	public function beforeExecuteRoute(Event $evt, Dispatcher $dispatcher) {
		$reModule = $dispatcher->getModuleName ();
		$reController = $dispatcher->getControllerName ();
		// handle module for working
		if ($this->session->has ( 'module' )) { // was login
			$userModule = $this->session->get ( 'module' );
			
			// login to other module, not your module/default module
			if ($reModule != $userModule && $reModule != Default_Module) {
				$this->flash->error ( 'Vùng bạn không có quyền truy câp. Đã chuyển về vùng mặc định' );
				$this->response->redirect ( '/' . $userModule . '/index/' );
				return false;
			}
		} else {
			// access to test when not login
			if ($reModule == Default_Module && $reController == 'test') {
				$this->flash->error ( 'Bạn cần đăng nhập để làm test' );
				return $this->response->redirect ( '/user/login/' );
			}
			
			// access to other module, which not is default module
			if ($reModule != Default_Module) {
				$this->flash->error ( 'Vùng bạn không có quyền truy câp. Đã chuyển về vùng mặc định' );
				return $this->response->redirect ( '/' . Default_Module . '/index/' );
			}
		}
		return true;
	}
}
?>