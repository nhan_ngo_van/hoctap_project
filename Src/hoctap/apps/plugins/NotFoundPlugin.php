<?php

namespace hoctap\Plugins;

use Phalcon\Mvc\User\Plugin;
use Phalcon\Events\Event;
use Phalcon\Dispatcher;
use Phalcon\Mvc\Dispatcher\Exception as DispatcherException;
use Phalcon\Mvc\Dispatcher as MVCDispatcher;

class NotFoundPlugin extends Plugin {
	public function beforeException(Event $event, MVCDispatcher $dispatcher, \Exception $exception) {
		error_log ( $exception->getMessage () . PHP_EOL . $exception->getTraceAsString () );
		if ($exception instanceof DispatcherException) {
			switch ($exception->getCode ()) {
				case Dispatcher::EXCEPTION_HANDLER_NOT_FOUND :
				case Dispatcher::EXCEPTION_ACTION_NOT_FOUND :
					$this->response->redirect ( '/anonymous/errors/show404?message=' . $exception->getMessage () );
					return false;
			}
		}
		$this->response->redirect ( '/anonymous/errors/show500?message=' . $exception->getMessage () );
		return false;
	}
}