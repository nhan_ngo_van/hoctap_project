<?php

namespace hoctap\Base\Controllers;

use Phalcon\Mvc\Controller;
use hoctap\Models\IgnoreCharacters;
require_once APP_PATH . '/apps/libraries/Mailer/PHPMailerAutoload.php';
require_once APP_PATH . '/apps/libraries/Facebook/autoload.php';
/**
 * web title
 *
 * @var string
 */
const WEB_TITLE = "Luyên tài ";
const MAIL_ADDRESS = 'luyentai.com@gmail.com';
const WEB_ADDRESS = 'https://luyentai.com';
const IS_STUDENT = 1;
const IS_TEACHER = 2;
/**
 *
 * @author nhannv-pc
 *         Base Controller
 */
class ControllerBase extends Controller {
	
	/**
	 * init function setup layout and title
	 */
	protected function initialize() {
		// set web title
		$this->tag->prependTitle ( WEB_TITLE );
		$this->assets->addJs('https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js');
		$this->assets->addJs('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js');
		$this->assets->addJs('/public/js/ajax/library.js');
		$this->assets->addJs('/public/js/main.js');
		$this->assets->addCss('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',false);
		$this->assets->addCss('/public/css/bootstrap/bootstrap.min.css',true);
		$this->assets->addCss('/public/css/common.css',true);
		$this->assets->addCss('/public/css/form.css',true);
		$this->assets->addCss('/public/img/webicon.png',true);
		$this->assets->addCss('/public/css/font-awesome/css/font-awesome.min.css',true);
	}
	protected function forward($uri) {
		$uriParts = explode ( '/', $uri );
		$params = array_slice ( $uriParts, 3 );
		return $this->dispatcher->forward ( array (
				'module' => $uriParts [0],
				'controller' => $uriParts [1],
				'action' => $uriParts [2],
				'param' => $params 
		) );
		;
	}
	public function getCommentModule($name, $id, $default) {
		$preview = '<div id="math_preview"></div>';
		$form = '<form method="get">' . '<textarea rows="3" cols="100" name="' . $name . '" required placeholder="bình luận" id="comment" onkeyup="updateFormula(this.value)" required>' . $default . '</textarea>' . '</form>';
		$form .= '<input type="submit" value="Trả lời" name="submit" class="comment-btn primary" onclick="postComment(\'' . $name . '\',\'' . $id . '\')">';
		$form .= '<script type="text/javascript">
					function updateFormula(value){
						var div = document.getElementById("math_preview");
						div.innerHTML = value;
						MathJax.Hub.Queue(["Typeset",MathJax.Hub,"math_preview"]);
					}
				</script>';
		return '<div class="none-print">' . $preview . $form . '</div>';
	}
	public function getControlPaginator($url, $page) {
		$controlPanigatorView = '<ul class="none-print pager">';
		$controlPanigatorView .= '<li>' . $this->tag->linkTo ( $url, ' <i class="fa fa-fast-backward" aria-hidden="true"></i>', array (
				'class' => 'btn' 
		) ) . '</li>';
		$controlPanigatorView .= '<li>' . $this->tag->linkTo ( $url . '?page=' . $page->before, '  <i class="fa fa-step-backward" aria-hidden="true"></i>', array (
				'class' => 'btn' 
		) ) . '</li>';
		$controlPanigatorView .= '<span class="help-inline"> ' . $page->current . ' / ' . $page->total_pages . ' </span>' . '</li>';
		$controlPanigatorView .= '<li>' . $this->tag->linkTo ( $url . '?page=' . $page->next, '  <i class="fa fa-step-forward" aria-hidden="true"></i>', array (
				'class' => 'btn' 
		) ) . '</li>';
		
		$controlPanigatorView .= '<li>' . $this->tag->linkTo ( $url . '?page=' . $page->last, '  <i class="fa fa-fast-forward" aria-hidden="true"></i>', array (
				'class' => 'btn' 
		) ) . '</li>';
		$controlPanigatorView .= '</ul>';
		return $controlPanigatorView;
	}
	/**
	 * Check invalid character
	 *
	 * @param string $contentChecking
	 *        	checking content
	 * @return invalid character if true
	 *         else return false
	 */
	protected function containInvalidChar($contentChecking) {
		$contentChecking = "Comment:" . $contentChecking;
		$ignoreChars = IgnoreCharacters::find ();
		foreach ( $ignoreChars as $char ) {
			if (strpos ( $contentChecking, $char->content )) {
				return $char->content;
			}
		}
		return false;
	}
	/**
	 * Generate password
	 *
	 * @param string $password        	
	 * @return string
	 */
	protected function getPassword($password) {
		$salt = "!@$^&*(^#!123abd24fhk";
		$context = hash_init ( "sha256", HASH_HMAC, $salt );
		
		hash_update ( $context, $password );
		return hash_final ( $context );
	}
	/**
	 * Send email by gmail
	 *
	 * @param string $fromEmail        	
	 * @param string $toEmail        	
	 * @param string $subject        	
	 * @param string $content        	
	 */
	protected function sendEmail($fromEmail, $toEmail, $subject, $content) {
		$mailer = new \PHPMailer ();
		$mailer->isSMTP ();
		$mailer->CharSet = "UTF-8";
		// $mail->SMTPDebug = 2;
		// $mail->Debugoutput = 'html';
		$mailer->Host = 'smtp.gmail.com';
		$mailer->Port = 587;
		$mailer->SMTPSecure = 'tls';
		$mailer->SMTPAuth = true;
		$mailer->Username = 'luyentai.com@gmail.com';
		$mailer->Password = 'plizxgvqltiqmojo';
		
		$mailer->setFrom ( utf8_decode ( $toEmail ), utf8_decode ( WEB_TITLE ) );
		$mailer->addReplyTo ( utf8_decode ( $toEmail ), utf8_decode ( WEB_TITLE ) );
		$added = $mailer->addAddress ( $toEmail, $toEmail );
		$mailer->IsHTML ( true );
		$mailer->Subject = $subject;
		
		$mailer->Body = $content;
		if ($mailer->send ()) {
			$this->flash->success ( 'Vui lòng kiểm tra email ' );
		} else {
			$this->flash->error ( "Xảy ra lỗi trong quá trình gửi email $toEmail, vui lòng thử lại sau<br/>" );
		}
	}
}
