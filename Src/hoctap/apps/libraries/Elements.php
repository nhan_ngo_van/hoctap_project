<?php

namespace hoctap\Libraries;

use Phalcon\Mvc\User\Component;

/**
 * Elements
 *
 * Helps to build UI elements for the application
 */
class Elements extends Component {
	private $_headerMenu = array (
			'navbar-left' => array (
					'Trang chủ' => array (
							'module' => 'anonymous',
							'controller' => 'index',
							'action' => 'index' 
					),
					'Bài giảng' => array (
							'module' => 'anonymous',
							'controller' => 'lesson',
							'action' => 'index' 
					),
					'Vào thi' => array (
							'module' => 'anonymous',
							'controller' => 'test',
							'action' => 'index' 
					),
					// 'Câu hỏi' => array (
					// 'module' => 'anonymous',
					// 'controller' => 'index',
					// 'action' => 'questions'
					// ),
					'Đề thi' => array (
							'module' => 'anonymous',
							'controller' => 'exam',
							'action' => 'index' 
					),
					'Forum' => array (
							'module' => 'anonymous',
							'controller' => 'forum',
							'action' => 'index' 
					) ,
					'Hướng dẫn' => array (
							'module' => 'anonymous',
							'controller' => 'help',
							'action' => 'syntax' 
					) 
			) 
	);
	// 'Liên hệ' => array (
	// 'module' => 'anonymous',
	// 'controller' => 'contact',
	// 'action' => 'index'
	// ),
	// 'Liên hệ' => array (
	// 'module' => 'anonymous',
	// 'controller' => 'about',
	// 'action' => 'index'
	// ),
	private $_leftSide = array (
			'Bài giảng' => array (
					'controller' => 'index',
					'action' => 'lessons',
					'any' => true 
			),
			'Câu hỏi' => array (
					'controller' => 'index',
					'action' => 'questions',
					'any' => true 
			),
			'Đề thi' => array (
					'controller' => 'index',
					'action' => 'exams',
					'any' => true 
			) 
	);
	
	/**
	 * Builds header menu with left and right items
	 *
	 * @return string
	 */
	public function getMenu() {
		$id = $this->session->get ( 'id' );
		$username = $this->session->get ( 'username' );
		if ($id && $username) {
			$this->_headerMenu ['navbar-right'] [''] = array (
					'module' => 'anonymous',
					'controller' => 'user',
					'action' => 'logout' 
			);
			// $this->_headerMenu ['navbar-left'] ['Cá nhân'] = array (
			// 'module' => $this->session->get ( 'module' ),
			// 'controller' => 'index',
			// 'action' => 'index'
			// );
		} else {
			$this->_headerMenu ['navbar-right'] ['Đăng nhập'] = array (
					'module' => 'anonymous',
					'controller' => 'user',
					'action' => 'login' 
			);
		}
		
		$controllerName = $this->view->getControllerName ();
		$actionName = $this->view->getActionName ();
		$moduleName = $this->dispatcher->getModuleName ();
		foreach ( $this->_headerMenu as $position => $menu ) {
			echo '<ul class="nav navbar-nav ' . $position . '">';
			foreach ( $menu as $caption => $option ) {
				if ($controllerName == $option ['controller'] && $moduleName == $option ['module'] ) {
					echo '<li class="active menu-item">';
				} else {
					if ($moduleName == $option ['module'] && $moduleName != 'anonymous') {
						echo '<li class="active menu-item">';
					} else {
						echo '<li class="menu-item">';
					}
				}
				// display user information
				$loginIcon = '';
				if ($position == 'navbar-right' && $caption != 'Đăng nhập') {
					$loginIcon = '<span class="glyphicon glyphicon-log-in" data-toggle="tooltip" title="Đăng xuất!"></span> ';
					// icon display
					$icon = $this->session->get ( 'icon' );
					echo '<img class="user-icon" src="' . $icon . '" width="50px" height="50px">' . '</li><li>';
					// User name
					$name = $this->session->get ( 'name' );
					
					echo $this->tag->linkTo ( $this->session->get ( 'module' ) . '/index/', $name ) . '</li><li>';
				}
				
				echo $this->tag->linkTo ( ($option ['module'] != 'anonymous' ? $option ['module'] : '') . '/' . $option ['controller'] . '/' . $option ['action'], $loginIcon . $caption );
				echo '</li>';
			}
			echo '</ul>';
		}
	}
	
	/**
	 * Returns menu tabs
	 */
	public function getTabs() {
		$controllerName = $this->view->getControllerName ();
		$actionName = $this->view->getActionName ();
		echo '<div class="col-md-2 left-bar none-print" role="complementary">';
		echo '<ul >';
		echo '<h3 align="center">Menu</h3>';
		foreach ( $this->_leftSide as $caption => $option ) {
			echo '<li >';
			echo $this->tag->linkTo ( $option ['controller'] . '/' . $option ['action'], $caption );
			echo '</li>';
		}
		echo '</ul></div>';
	}
	/**
	 * Get link to parent of this view
	 */
	public function getParent() {
		$controller = $this->view->getControllerName ();
		$action = $this->view->getActionName ();
		$module = $this->dispatcher->getModuleName ();
		$viewModule = 'Home';
		switch ($module) {
			case 'anonymous' :
				$viewModule = 'Home';
				break;
			case 'teacher' :
				$viewModule = 'Giáo Viên';
				break;
			case 'admin' :
				$viewModule = 'Quản trị';
				break;
			case 'student' :
				$viewModule = 'Học sinh';
				break;
		}
		echo '<li>';
		echo $this->tag->linkTo ( '/' . $module, $viewModule );
		echo '</li>';
		if ($controller != 'index' || $action != 'index') {
			echo '<li>';
			echo $this->tag->linkTo ( '/' . $module . '/' . $controller, $controller );
			echo '</li>';
		}
		if ($action != 'index') {
			echo '<li>';
			echo $this->tag->linkTo ( '/' . $module . '/' . $controller . '/' . $action, $action );
			echo '</li>';
		}
	}
}
