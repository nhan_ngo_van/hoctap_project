<?php

namespace hoctap\Admin\Models;

use Phalcon\Mvc\Model\Validator\Email as Email;

class Users extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $first_name;
	
	/**
	 *
	 * @var string
	 */
	public $user_name;
	
	/**
	 *
	 * @var string
	 */
	public $password;
	
	/**
	 *
	 * @var string
	 */
	public $last_name;
	
	/**
	 *
	 * @var string
	 */
	public $mobile_phone;
	
	/**
	 *
	 * @var string
	 */
	public $bithday;
	
	/**
	 *
	 * @var string
	 */
	public $email;
	
	/**
	 *
	 * @var string
	 */
	public $facebook;
	
	/**
	 *
	 * @var string
	 */
	public $address;
	
	/**
	 *
	 * @var string
	 */
	public $school;
	
	/**
	 *
	 * @var integer
	 */
	public $education_level;
	
	/**
	 *
	 * @var integer
	 */
	public $rank;
	
	/**
	 *
	 * @var string
	 */
	public $join_date;
	
	/**
	 *
	 * @var integer
	 */
	public $user_type;
	
	/**
	 *
	 * @var integer
	 */
	public $active;
	
	/**
	 *
	 * @var string
	 */
	public $icon_url;
	
	/**
	 *
	 * @var string
	 */
	public $block_until_date;
	
	/**
	 * Validations and business logic
	 *
	 * @return boolean
	 */
	public function validation() {
		$this->validate ( new Email ( array (
				'field' => 'email',
				'required' => true 
		) ) );
		
		if ($this->validationHasFailed () == true) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->hasMany ( 'id', 'Exams', 'author_id', array (
				'alias' => 'Exams' 
		) );
		$this->hasMany ( 'id', 'Questions', 'author_id', array (
				'alias' => 'Questions' 
		) );
		$this->hasMany ( 'id', 'QuestionsAnswers', 'author_id', array (
				'alias' => 'QuestionsAnswers' 
		) );
		$this->belongsTo ( 'user_type', 'UserTypes', 'id', array (
				'alias' => 'UserTypes' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Users[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Users
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'users';
	}
}
