<?php

namespace hoctap\Admin\Controllers;

use hoctap\Models\Topics;
use hoctap\Base\Controllers\ControllerBase;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Forms\TopicForm;

class TopicController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
	}
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		$topics = Topics::find ( array (
				'order' => 'create_date desc' 
		) );
		// 'conditions' => 'author_id =?1',
		// 'bind' => array (
		// 1 => $this->session->get ( 'id' )
		// )
		
		// 'limit' => LIMIT_VIEW
		
		$this->view->topics = $topics;
		$paginator = new Paginator ( array (
				'data' => $topics,
				'limit' => 20,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/admin/topic/', $this->view->page );
	}
	/**
	 *
	 * @param integer $id:
	 *        	update topic
	 */
	public function updateAction($id) {
		if ($this->session->has ( 'id' ) && $this->session->has ( 'username' )) {
			$topic = Topics::findFirstById ( $id );
			if ($topic && $topic->author_id == $this->session->get ( 'id' )) {
				$form = new TopicForm ( $topic, array (
						'edit' => true 
				) );
				$this->view->form = $form;
			} else {
				$this->flash->error ( 'Không tìm thấy topic của bạn' );
				$this->response->redirect ( '/user/login/' );
			}
		} else {
			$this->flash->error ( 'Bạn phải đăng nhập mới có sửa topic' );
			$this->response->redirect ( '/user/login/' );
		}
	}
	public function saveAction($id) {
		if ($this->request->isPost ()) {
			if ($this->request->hasPost ( 'id' )) {
				$id = $this->request->getPost ( 'id' );
				$topic = Topics::findFirstById ( $id );
				if ($topic && $topic->author_id == $this->session->get ( 'id' )) {
					$form = new TopicForm ();
					$data = $this->request->getPost ();
					if ($form->isValid ( $data, $topic )) {
						$topic->date_modify = date ( 'Y-m-d H:i:s' );
						if ($topic->save ()) {
							$this->flash->success ( 'Cập nhật thành công' );
							return $this->response->redirect ( '/forum/topic/' . $id );
						}
					}
				}
			}
			$this->flash->error ( 'Cập nhật không thành công' );
			$this->response->redirect ( '/forum/topic/' . $id );
		}
	}
	/**
	 *
	 * @param integer $id:
	 *        	id of topic
	 */
	public function deleteAction($id) {
		$topic = Topics::findFirstById ( $id );
		// validate permission only owner can delete
		// if ($topic && $topic->author_id == $this->session->get ( 'id' )) {
		
		if (! $topic->delete ()) {
			$this->flash->error ( 'Có lỗi trong quá trình xóa' );
		} else {
			$this->flash->success ( 'Đã xóa: ' . $topic->name );
		}
		// }
		return $this->response->redirect ( '/admin/topic/' );
	}
}

