<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\IgnoreCharacters;
use hoctap\Forms\IgnoreCharacterForm;

const ForwardIndex = '/ignore-character/index';
const DirectIndex = '/admin' . ForwardIndex;
/**
 * Manage question type
 *
 * @author nhannv-pc
 *        
 */
class IgnoreCharacterController extends ControllerBase {
	public function initialize() {
		$this->tag->setTitle ( 'Ký tự không được phép' );
		parent::initialize ();
	}
	/**
	 * Create/delete question type
	 */
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( 'page', 'int' );
		}
		$ingChar = IgnoreCharacters::find ();
		if (count ( $ingChar ) == 0) {
			$this->flash->notice ( "Did not find any ingore character" );
		}
		$paginator = new Paginator ( array (
				'data' => $ingChar,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
	}
	public function createAction() {
		$this->tag->setTitle ( 'Tạo mới chủ đề' );
		$ingCharForm = new IgnoreCharacterForm ( null );
		if ($this->request->isPost ()) {
			
			$ingChar = new IgnoreCharacters ();
			$data = $this->request->getPost ();
			
			if (! $ingCharForm->isValid ( $data, $ingChar )) {
				foreach ( $ingCharForm->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardIndex );
			}
			if ($ingChar->save () == false) {
				foreach ( $ingChar->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardIndex );
			}
			$this->flash->success ( "Tạo thành công" );
			$this->response->redirect ( DirectIndex );
		}
		$this->view->form = $ingCharForm;
	}
	public function updateAction($id) {
		$this->tag->setTitle ( 'Cập nhật chủ đề' );
		if (! $this->request->isPost ()) {
			$ingChar = IgnoreCharacters::findFirstById ( $id );
			if (! $ingChar) {
				$this->flash->error ( 'Chủ đề không tìm thấy' . $id );
			}
			$this->view->form = new IgnoreCharacterForm ( $ingChar, array (
					'edit' => true 
			) );
		}
	}
	public function deleteAction($id) {
		$ingChar = IgnoreCharacters::findFirstById ( $id );
		if (! $ingChar) {
			$this->flash->error ( "Ký tự không tồn tại" );
			return $this->forward ( ForwardIndex );
		}
		if (! $ingChar->delete ()) {
			foreach ( $ingChar->getMessages () as $message ) {
				$this->flash->error ( $message );
			}
		}
		$this->flash->success ( "Đã xóa" );
		return $this->response->redirect ( DirectIndex );
	}
	public function saveAction() {
		if ($this->request->isPost ()) {
			if (! $this->request->hasPost ( 'id' )) {
				$this->flash->error ( 'Không có tham số' );
				return $this->forward ( ForwardIndex );
			}
			$id = $this->request->getPost ( 'id', 'int' );
			$ingChar = IgnoreCharacters::findFirstById ( $id );
			if ($ingChar) {
				$data = $this->request->getPost ();
				
				$form = new IgnoreCharacterForm ();
				$this->view->form = $form;
				
				if ($form->isValid ( $data, $ingChar )) {
					if ($ingChar->save () == false) {
						foreach ( $ingChar->getMessages () as $message ) {
							$this->flash->error ( $message );
						}
						return $this->forward ( '/ingore-character/update/' . $id );
					}
					$this->flash->success ( 'Cập nhật thành công' );
					return $this->response->redirect ( DirectIndex );
				} else {
					$this->flash->success ( 'Mẫu không khớp' );
				}
			} else {
				$this->flash->error ( 'Không tìm thấy chủ đề' );
				return $this->forward ( ForwardIndex );
			}
		} else {
			return $this->response->redirect ( DirectIndex );
		}
	}
}

