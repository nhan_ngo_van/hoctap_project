<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Questions;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\CommentsOfQuestions;
use hoctap\Models\CommentsOfLessons;
use hoctap\Models\CommentsOfExams;

/**
 * Link to create question
 *
 * @var string
 * @author nhannv-pc
 *        
 */
const LinkCommentIndex = '/comment/index/';
class CommentController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->tag->prependTitle ( 'Quản lý câu hỏi ' );
	}
	/**
	 * Index action to manage question
	 */
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$questionComment = CommentsOfQuestions::find ();
		$paginatorQ = new Paginator ( array (
				'data' => $questionComment,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->pageQ = $paginatorQ->getPaginate ();
		
		// lesson
		$lessonComment = CommentsOfLessons::find ();
		$paginatorL = new Paginator ( array (
				'data' => $lessonComment,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->pageL = $paginatorL->getPaginate ();
		
		// exam
		$examComment = CommentsOfExams::find ();
		$paginatorE = new Paginator ( array (
				'data' => $examComment,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->pageE = $paginatorE->getPaginate ();
	}
	/**
	 * Delete question by id
	 *
	 * @param int $id        	
	 */
	public function deleteAction($id, $kind) {
		$comment = null;
		switch ($kind) {
			case 'question' :
				$comment = CommentsOfQuestions::findFirstById ( $id );
				break;
			case 'lesson' :
				$comment = CommentsOfLessons::findFirstById ( $id );
				break;
			case 'exam' :
				$comment = CommentsOfExams::findFirstById ( $id );
				break;
		}
		if ($comment) {
			// validate permission only owner can delete
			if (! $comment->delete ()) {
				foreach ( $products->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
			}
			$this->flash->success ( 'Đã xóa' );
		} else {
			$this->flash->error ( 'Không tìm thấy comment/hoặc đã xóa' );
		}
		return $this->forward ( LinkCommentIndex );
	}
	/**
	 * View the question
	 *
	 * @param unknown $id        	
	 */
	public function viewAction($id) {
		if (! $this->request->isPost ()) {
			$question = Questions::findFirstById ( $id );
			if ($question) {
				$this->view->question = $question;
			} else {
				$this->flash->error ( 'Không tìm thấy câu hỏi' );
			}
		}
	}
}

