<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Questions;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Forms\QuestionSearchForm;

/**
 * Link to create question
 *
 * @var string
 * @author nhannv-pc
 *        
 */
const LinkQuestionCreate = '/question/create';
const LinkQuestionIndex = '/question/index/';
class QuestionController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->tag->prependTitle ( 'Quản lý câu hỏi ' );
	}
	/**
	 * Index action to manage question
	 */
	public function indexAction() {
		// handle request for paginator
		$this->view->searchQuestionForm = new QuestionSearchForm ();
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$questions = Questions::find ();
		if (count ( $questions ) == 0) {
			$this->flash->notice ( "Did not find any questions" );
		}
		$paginator = new Paginator ( array (
				'data' => $questions,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
	}
	/**
	 * Delete question by id
	 *
	 * @param int $id        	
	 */
	public function deleteAction($id) {
		$question = Questions::findFirstById ( $id );
		if ($question) {
			// validate permission only owner can delete
			if (! $question->delete ()) {
				foreach ( $products->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
			}
			$this->flash->success ( 'Đã xóa: ' . $question->question_name );
		} else {
			$this->flash->error ( 'Không tìm thấy câu hỏi' );
		}
		return $this->forward ( LinkQuestionIndex );
	}
	/**
	 * View the question
	 *
	 * @param unknown $id        	
	 */
	public function viewAction($id) {
		if (! $this->request->isPost ()) {
			$question = Questions::findFirstById ( $id );
			if ($question) {
				$this->view->question = $question;
			} else {
				$this->flash->error ( 'Không tìm thấy câu hỏi' );
			}
		}
	}
	public function questionAction() {
		$this->view->disable ();
		$response = new \Phalcon\Http\Response ();
		if ($this->request->has ( 'value' )) {
			echo 'after php:' . $this->request->get ( 'value' );
		} else {
			echo 'after php not have:' . $this->request->get ( 'value' );
		}
	}
}

