<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Forms\UserForm;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Admin\Models\Users;
use hoctap\Models\TRequest;

const ForwardIndex = '/user/index';
const DirectIndex = '/admin' . ForwardIndex;
/**
 * Manage question type
 *
 * @author nhannv-pc
 *        
 */
class UserController extends ControllerBase {
	public function initialize() {
		$this->tag->setTitle ( 'Người dùng' );
		parent::initialize ();
		$this->assets->addJs ( 'public/js/admin/user.js' );
	}
	/**
	 * Manager user: list all user to update, delete,and create new
	 */
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( 'page', 'int' );
		}
		$user = Users::find ();
		if (count ( $user ) == 0) {
			$this->flash->notice ( "Did not find any questions" );
		}
		$paginator = new Paginator ( array (
				'data' => $user,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
	}
	/**
	 * Create new user account
	 *
	 * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
	 */
	public function createAction() {
		$this->tag->setTitle ( 'Thêm người dùng' );
		$userForm = new UserForm ( null );
		
		if ($this->request->isPost ()) {
			// Validate account: username, email is not dublicate with other account
			$found = Users::find ( array (
					'conditions' => 'user_name =?1 or email = ?2',
					'bind' => array (
							1 => $this->request->getPost ( 'user_name' ),
							2 => $this->request->getPost ( 'email' ) 
					) 
			) );
			if (count ( $found ) > 0) {
				$this->flash->error ( 'Tài khoản hoặc email đã được đăng ký' );
				return $this->response->redirect ( '/admin/user/' );
			}
			
			// Create new user
			$user = new Users ();
			$data = $this->request->getPost ();
			
			if (! $userForm->isValid ( $data, $user )) {
				foreach ( $userForm->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardIndex );
			}
			$user->password = md5 ( $user->password );
			$user->join_date = date ( 'Y-m-d' );
			if ($user->save () == false) {
				foreach ( $user->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardIndex );
			}
			$this->flash->success ( "Tạo thành công" );
			$this->response->redirect ( DirectIndex );
		}
		
		// set form to view
		$this->view->form = $userForm;
	}
	/**
	 * Form to update new user
	 *
	 * @param integer $id        	
	 */
	public function updateAction($id) {
		$this->tag->setTitle ( 'Cập nhật user' );
		if (! $this->request->isPost ()) {
			$user = Users::findFirstById ( $id );
			if (! $user) {
				$this->flash->error ( 'Không tìm thấy user có ID:' . $id );
			}
			$this->view->form = new UserForm ( $user, array (
					'edit' => true 
			) );
		}
	}
	/**
	 * Delete user
	 *
	 * @param integer $id        	
	 * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
	 */
	public function deleteAction($id) {
		$user = Users::findFirstById ( $id );
		if (! $user) {
			$this->flash->error ( "Người dùng đã bị xóa hoặc không tồn tại" );
			return $this->forward ( ForwardIndex );
		}
		if (! $user->delete ()) {
			foreach ( $user->getMessages () as $message ) {
				$this->flash->error ( $message );
			}
		}
		$this->flash->success ( "Đã xóa người dùng có ID:" . $id );
		return $this->response->redirect ( DirectIndex );
	}
	/**
	 * Display detail information of a user
	 *
	 * @param unknown $id        	
	 */
	public function viewAction($id) {
		if (! $this->request->isPost ()) {
			$user = Users::findFirstById ( $id );
			if ($user) {
				$this->view->user = $user;
			} else {
				$this->flash->error ( 'Không tìm thấy user' );
				$this->forward ( ForwardIndex );
			}
		}
	}
	/**
	 * save information of user after input in form
	 *
	 * @return \Phalcon\Http\Response|\Phalcon\Http\ResponseInterface
	 */
	public function saveAction() {
		if ($this->request->isPost ()) {
			if (! $this->request->hasPost ( 'id' )) {
				$this->flash->error ( 'Không có tham số' );
				return $this->forward ( ForwardIndex );
			}
			$id = $this->request->getPost ( 'id', 'int' );
			$user = Users::findFirstById ( $id );
			if ($user) {
				$data = $this->request->getPost ();
				
				$form = new UserForm ();
				$this->view->form = $form;
				
				if ($form->isValid ( $data, $user )) {
					if ($user->save () == false) {
						foreach ( $user->getMessages () as $message ) {
							$this->flash->error ( $message );
						}
						return $this->forward ( '/user/update/' . $id );
					}
					$this->flash->success ( 'Cập nhật thành công' );
					return $this->response->redirect ( DirectIndex );
				} else {
					$this->flash->success ( 'Mẫu không khớp' );
				}
			} else {
				$this->flash->error ( 'Không tìm thấy user' );
				return $this->forward ( ForwardIndex );
			}
		} else {
			return $this->response->redirect ( DirectIndex );
		}
	}
	/**
	 * Display list request teacher account
	 */
	public function changeAction() {
		$requestModel = TRequest::find ();
		if (count ( $requestModel ) > 0) {
			$this->view->list = $requestModel;
		} else {
			$this->flash->error ( 'Không có yêu cầu nào' );
			$this->view->list = null;
		}
	}
	/**
	 * Aprove request become teacher or not
	 * 
	 * @param unknown $user_id        	
	 */
	public function aproveAction($user_id) {
		if ($this->request->isPost ()) {
		} else {
			$user = Users::findFirstById ( $user_id );
			$requestDetail = TRequest::find ( array (
					'conditions' => 'id = ?1',
					'bind' => array (
							1 => $user_id 
					) 
			) );
			
			$this->view->user = $user;
			$this->view->requestDetail = $requestDetail [0];
		}
	}
}

