<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\WebInfo;
use hoctap\Forms\InfoForm;

/**
 * Link to create question
 *
 * @var string
 * @author nhannv-pc
 *        
 */
class InfoController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->tag->prependTitle ( 'Tag - ' );
		$this->assets->addJs ( '/public/js/editer/full/ckeditor.js' );
	}
	/**
	 * Index action to manage question
	 */
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$tags = WebInfo::find ();
		if (count ( $tags ) == 0) {
			$this->flash->notice ( "Did not find any tags" );
		}
		$paginator = new Paginator ( array (
				'data' => $tags,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/admin/info/index/', $this->view->page );
	}
	/**
	 * Create action to create new question
	 */
	public function createAction() {
		$this->tag->setTitle ( 'Tạo 1 tag mới' );
		$infoForm = new InfoForm ( null );
		if ($this->request->isPost ()) {
			if ($infoForm->isValid ( $this->request->getPost () ) != false) {
				$infoModel = new WebInfo ();
				// get value from form
				$infoModel->tag = $this->request->getPost ( 'tag' );
				$infoModel->content = $this->request->getPost ( 'content' );
				
				if (! $infoModel->save ()) {
					$this->flash->error ( 'Tạo tag không thành công' );
					return $this->forward ( '/admin/info/' );
				} else {
					
					$this->flash->success ( 'Tạo tag thành công' );
					return $this->response->redirect ( '/admin/info/index/' . $infoModel->id );
				}
			}
		}
		$this->view->form = $infoForm;
	}
	/**
	 * Form update question
	 *
	 * @param int $tag
	 *        	:id of question
	 */
	public function updateAction($tag) {
		$this->tag->setTitle ( 'Cập nhật Tag ' );
		if (! $this->request->isPost ()) {
			$items = WebInfo::findFirstByTag ( $tag );
			if (! $items) {
				$this->flash->error ( 'Tag không tìm thấy' . $tag );
			}
			// view form
			$this->view->question = $items;
			$this->view->form = new InfoForm ( $items, array (
					'edit' => true 
			) );
		}
	}
	/**
	 * Delete question by id
	 *
	 * @param int $id        	
	 */
	public function deleteAction($id) {
		$item = WebInfo::findFirstByTag ( $id );
		if ($item) {
			// validate permission only owner can delete
			// if ($item->author_id == $this->session->get ( 'id' )) {
			if (! $item->delete ()) {
				// foreach ( $products->getMessages () as $message ) {
				$this->flash->error ( 'Xóa không thành công' );
				// }
			}
			$this->flash->success ( 'Đã xóa: ' . $item->tag );
			// }
		} else {
			$this->flash->error ( 'Không tìm thấy tag' );
		}
		$this->forward ( '/info/' );
	}
	/**
	 * Save updated data
	 */
	public function saveAction() {
		if ($this->request->isPost ()) {
			if (! $this->request->hasPost ( 'tag' )) {
				$this->flash->error ( 'Không có tham số' );
				return $this->forward ( '/admin/info/' );
			}
			$id = $this->request->getPost ( 'tag' );
			$item = WebInfo::findFirstByTag ( $id );
			if ($item) {
				$data = $this->request->getPost ();
				
				$form = new InfoForm ();
				$this->view->form = $form;
				
				if ($form->isValid ( $data, $item )) {
					if ($item->save () == false) {
						foreach ( $item->getMessages () as $message ) {
							$this->flash->error ( $message );
						}
						return $this->forward ( '/info/update/' . $id );
					}
					$this->flash->success ( 'Cập nhật thành công' );
					return $this->response->redirect ( '/admin/info/' );
				} else {
					$this->flash->success ( 'Mẫu không khớp' );
				}
			} else {
				$this->flash->error ( 'Không tìm thấy tag' );
				return $this->forward ( '/info/index/' );
			}
		} else {
			return $this->response->redirect ( '/admin/info/' );
		}
	}
}

