<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Categories;
use hoctap\Forms\CategoryForm;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
 * Manage question type
 *
 * @author nhannv-pc
 *        
 */
class CategoryController extends ControllerBase {
	public function initialize() {
		$this->tag->setTitle ( "Kiểu câu hỏi" );
		parent::initialize ();
	}
	/**
	 * Create/delete question type
	 */
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( 'page', 'int' );
		}
		$categories = Categories::find ();
		if (count ( $categories ) == 0) {
			$this->flash->notice ( "Did not find any questions" );
		}
		$paginator = new Paginator ( array (
				'data' => $categories,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
	}
	public function createAction() {
		$this->tag->setTitle ( 'Tạo mới chủ đề' );
		$categoryForm = new CategoryForm ( null );
		if ($this->request->isPost ()) {
			
			$category = new Categories ();
			$data = $this->request->getPost ();
			
			if (! $categoryForm->isValid ( $data, $category )) {
				foreach ( $categoryForm->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( 'category/index' );
			}
			if ($category->save () == false) {
				foreach ( $category->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( 'category/index' );
			}
			$this->flash->success ( "Tạo thành công" );
			$this->response->redirect ( '/admin/category/index' );
		}
		$this->view->form = $categoryForm;
	}
	public function updateAction($id) {
		$this->tag->setTitle ( 'Cập nhật chủ đề' );
		if (! $this->request->isPost ()) {
			$category = Categories::findFirstById ( $id );
			if (! $category) {
				$this->flash->error ( 'Chủ đề không tìm thấy' . $id );
			}
			$this->view->form = new CategoryForm ( $category, array (
					'edit' => true 
			) );
		}
	}
	public function deleteAction($id) {
		$category = Categories::findFirstById ( $id );
		if (! $category) {
			$this->flash->error ( "Chủ đề không tồn tại" );
			return $this->forward ( "/category/index" );
		}
		if (! $category->delete ()) {
			foreach ( $category->getMessages () as $message ) {
				$this->flash->error ( $message );
			}
		}
		$this->flash->success ( "Đã xóa" );
		return $this->response->redirect ( '/admin/' . $this->router->getControllerName () . '/index' );
	}
	public function saveAction($id) {
		if ($this->request->isPost ()) {
			if (! $this->request->hasPost ( 'id' )) {
				$this->flash->error ( 'Không có tham số' );
				return $this->forward ( 'category/index' );
			}
			$id = $this->request->getPost ( 'id', 'int' );
			$category = Categories::findFirstById ( $id );
			if ($category) {
				$data = $this->request->getPost ();
				
				$form = new CategoryForm ();
				$this->view->form = $form;
				
				if ($form->isValid ( $data, $category )) {
					if ($category->save () == false) {
						foreach ( $category->getMessages () as $message ) {
							$this->flash->error ( $message );
						}
						return $this->forward ( '/category/update/' . $id );
					}
					$form->clear ();
					$this->flash->success ( 'Cập nhật thành công' );
					return $this->response->redirect ( '/admin/' . $this->router->getControllerName () . '/index' );
				} else {
					$this->flash->success ( 'Mẫu không khớp' );
				}
			} else {
				$this->flash->error ( 'Không tìm thấy chủ đề' );
				return $this->forward ( 'category/index' );
			}
		} else {
			return $this->response->redirect ( '/admin/' . $this->router->getControllerName () . '/index' );
		}
	}
	/**
	 * Get list question type and put to table
	 *
	 * @return string: html tag including table question type
	 */
	private function getListQuestionTypes() {
		
		// get all question type from database
		$categories = Categories::find ();
		
		// init table for html view
		$html = '<table border="1" >';
		$html .= '<tr> <th> ID</th> <th> Acronym</th><th> Kiểu</th>  <th> Xóa </th></tr>';
		
		// create table for type of question
		foreach ( $categories as $category ) {
			$html .= '<tr>';
			
			// create link to update/delete
			$linkUpdate = 'admin/' . $this->router->getControllerName () . '/update/' . $category->id;
			$linkDelete = 'admin/' . $this->router->getControllerName () . '/delete/' . $category->id;
			
			$html = $html . '<td>' . $category->id . '</td><td> ' . $this->tag->linkTo ( $linkUpdate, $category->acronym ) . '</td><td>' . $category->type . '</td>';
			
			// append delete link
			$html .= '<td>' . $this->tag->linkTo ( $linkDelete, 'Del' ) . '</td>';
		}
		$html .= '</tr></table>';
		return $html;
	}
}

