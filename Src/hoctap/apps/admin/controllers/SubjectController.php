<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Forms\SubjectForm;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Admin\Models\Subjects;

const ForwardIndex = '/subject/index';
const DirectIndex = '/admin' . ForwardIndex;
/**
 * Manage question type
 *
 * @author nhannv-pc
 *        
 */
class SubjectController extends ControllerBase {
	public function initialize() {
		$this->tag->setTitle ( 'Môn học' );
		parent::initialize ();
	}
	/**
	 * Create/delete question type
	 */
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( 'page', 'int' );
		}
		$subjects = Subjects::find ();
		if (count ( $subjects ) == 0) {
			$this->flash->notice ( "Did not find any questions" );
		}
		$paginator = new Paginator ( array (
				'data' => $subjects,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
	}
	public function createAction() {
		$this->tag->setTitle ( 'Tạo mới chủ đề' );
		$subjectForm = new SubjectForm ( null );
		if ($this->request->isPost ()) {
			
			$subject = new Subjects ();
			$data = $this->request->getPost ();
			
			if (! $subjectForm->isValid ( $data, $subject )) {
				foreach ( $subjectForm->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardIndex );
			}
			if ($subject->save () == false) {
				foreach ( $subject->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardIndex );
			}
			$this->flash->success ( "Tạo thành công" );
			$this->response->redirect ( DirectIndex );
		}
		$this->view->form = $subjectForm;
	}
	public function updateAction($id) {
		$this->tag->setTitle ( 'Cập nhật chủ đề' );
		if (! $this->request->isPost ()) {
			$subject = Subjects::findFirstById ( $id );
			if (! $subject) {
				$this->flash->error ( 'Chủ đề không tìm thấy' . $id );
			}
			$this->view->form = new SubjectForm ( $subject, array (
					'edit' => true 
			) );
		}
	}
	public function deleteAction($id) {
		$subject = Subjects::findFirstById ( $id );
		if (! $subject) {
			$this->flash->error ( "Môn học không tồn tại" );
			return $this->forward ( ForwardIndex );
		}
		if (! $subject->delete ()) {
			foreach ( $subject->getMessages () as $message ) {
				$this->flash->error ( $message );
			}
		}
		$this->flash->success ( "Đã xóa" );
		return $this->response->redirect ( DirectIndex );
	}
	public function saveAction() {
		if ($this->request->isPost ()) {
			if (! $this->request->hasPost ( 'id' )) {
				$this->flash->error ( 'Không có tham số' );
				return $this->forward ( ForwardIndex );
			}
			$id = $this->request->getPost ( 'id', 'int' );
			$subject = Subjects::findFirstById ( $id );
			if ($subject) {
				$data = $this->request->getPost ();
				
				$form = new SubjectForm ();
				$this->view->form = $form;
				
				if ($form->isValid ( $data, $subject )) {
					if ($subject->save () == false) {
						foreach ( $subject->getMessages () as $message ) {
							$this->flash->error ( $message );
						}
						return $this->forward ( '/subject/update/' . $id );
					}
					$this->flash->success ( 'Cập nhật thành công' );
					return $this->response->redirect ( DirectIndex );
				} else {
					$this->flash->success ( 'Mẫu không khớp' );
				}
			} else {
				$this->flash->error ( 'Không tìm thấy chủ đề' );
				return $this->forward ( ForwardIndex );
			}
		} else {
			return $this->response->redirect ( DirectIndex );
		}
	}
}

