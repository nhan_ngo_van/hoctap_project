<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;

/**
 * Link to question index controller
 *
 * @var string
 */
const LinkQuestionType = 'admin/category/index';

/**
 * Link to manage user
 *
 * @var string
 */
const LinkUserManage = 'admin/user/index';

/**
 * Index page for admin to manage
 *
 * @author nhannv-pc
 *        
 */
const LinkSubjectManage = 'admin/subject/';
const LinkUserTypeManage = 'admin/user-type/';
const LinkQuestionManage = 'admin/question/';
const LinkIngoreCharacterManage = 'admin/ignore-character/';
const LinkExamManage = 'admin/exam/';
const LinkLessonManage = 'admin/lesson/';
const LinkCommentManage = 'admin/comment';
class IndexController extends ControllerBase {
	public function initialize() {
		$this->tag->setTitle ( "Admin Management" );
		parent::initialize ();
	}
	public function indexAction() {
		$this->tag->setTitle ( "Trang quản trị viên" );
	}
}

