<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Admin\Models\UserTypes;
use hoctap\Forms\UserTypeForm;

const ForwardIndex = '/user-type/index';
const DirectIndex = '/admin' . ForwardIndex;
/**
 * Manage question type
 *
 * @author nhannv-pc
 *        
 */
class UserTypeController extends ControllerBase {
	public function initialize() {
		$this->tag->setTitle ( 'Môn học' );
		parent::initialize ();
	}
	/**
	 * Create/delete question type
	 */
	public function indexAction() {
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( 'page', 'int' );
		}
		$userType = UserTypes::find ();
		if (count ( $userType ) == 0) {
			$this->flash->notice ( "Did not find any questions" );
		}
		$paginator = new Paginator ( array (
				'data' => $userType,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
	}
	public function createAction() {
		$this->tag->setTitle ( 'Tạo mới chủ đề' );
		$userTypeForm = new UserTypeForm ( null );
		if ($this->request->isPost ()) {
			
			$userType = new UserTypes ();
			$data = $this->request->getPost ();
			
			if (! $userTypeForm->isValid ( $data, $userType )) {
				foreach ( $userTypeForm->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardIndex );
			}
			if ($userType->save () == false) {
				foreach ( $userType->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardIndex );
			}
			$this->flash->success ( "Tạo thành công" );
			$this->response->redirect ( DirectIndex );
		}
		$this->view->form = $userTypeForm;
	}
	public function updateAction($id) {
		$this->tag->setTitle ( 'Cập nhật chủ đề' );
		if (! $this->request->isPost ()) {
			$userType = UserTypes::findFirstById ( $id );
			if (! $userType) {
				$this->flash->error ( 'Chủ đề không tìm thấy' . $id );
			}
			$this->view->form = new UserTypeForm ( $userType, array (
					'edit' => true 
			) );
		}
	}
	public function deleteAction($id) {
		$userType = UserTypes::findFirstById ( $id );
		if (! $userType) {
			$this->flash->error ( "Kiểu không tồn tại" );
			return $this->forward ( ForwardIndex );
		}
		if (! $userType->delete ()) {
			foreach ( $userType->getMessages () as $message ) {
				$this->flash->error ( $message );
			}
		}
		$this->flash->success ( "Đã xóa" );
		return $this->response->redirect ( DirectIndex );
	}
	public function saveAction() {
		if ($this->request->isPost ()) {
			if (! $this->request->hasPost ( 'id' )) {
				$this->flash->error ( 'Không có tham số' );
				return $this->forward ( ForwardIndex );
			}
			$id = $this->request->getPost ( 'id', 'int' );
			$userType = UserTypes::findFirstById ( $id );
			if ($userType) {
				$data = $this->request->getPost ();
				
				$form = new UserTypeForm ();
				$this->view->form = $form;
				
				if ($form->isValid ( $data, $userType )) {
					if ($userType->save () == false) {
						foreach ( $userType->getMessages () as $message ) {
							$this->flash->error ( $message );
						}
						return $this->forward ( '/user-type/update/' . $id );
					}
					$this->flash->success ( 'Cập nhật thành công' );
					return $this->response->redirect ( DirectIndex );
				} else {
					$this->flash->success ( 'Mẫu không khớp' );
				}
			} else {
				$this->flash->error ( 'Không tìm thấy chủ đề' );
				return $this->forward ( ForwardIndex );
			}
		} else {
			return $this->response->redirect ( DirectIndex );
		}
	}
}

