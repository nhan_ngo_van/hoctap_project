<?php

namespace hoctap\Admin\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Admin\Models\Users;
use hoctap\Models\TRequest;
use const hoctap\Base\Controllers\IS_TEACHER;

/**
 * Manage question type
 *
 * @author nhannv-pc
 *        
 */
class AjaxController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->view->disable ();
	}
	
	/**
	 * Accept teacher account for $id
	 */
	public function acceptRequestAction() {
		$return;
		if ($this->request->isPost ()) {
			$user_id = $this->request->getPost ( "user-id" );
			$user = Users::findFirstById ( $user_id );
			$requestDetail = TRequest::find ( array (
					'conditions' => 'id = ?1',
					'bind' => array (
							1 => $user_id 
					) 
			) );
			$user->user_type = IS_TEACHER;
			if ($user->save ()) {
				if (! $requestDetail->delete ()) {
					error_log ( 'cannot delete request' . $user_id );
				}
				$return = '/admin/user/view/' . $user_id;
				echo $return;
			} else {
				foreach ( $user->getMessages () as $message ) {
					error_log ( $message );
				}
			}
		}
	}
	/**
	 * denied teacher account for $id
	 */
	public function denyRequestAction() {
		if ($this->request->isPost ()) {
			$user_id = $this->request->getPost ( "user-id" );
			$requestDetail = TRequest::find ( array (
					'conditions' => 'id = ?1',
					'bind' => array (
							1 => $user_id 
					) 
			) );
			if (! $requestDetail->delete ()) {
				foreach ( $requestDetail->getMessages () as $message ) {
					error_log ( $message );
				}
			}
			echo '/admin/user/change';
		}
	}
}

