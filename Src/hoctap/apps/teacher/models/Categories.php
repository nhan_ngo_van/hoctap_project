<?php

namespace hoctap\Teacher\Models;

class Categories extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	protected $id;
	
	/**
	 *
	 * @var string
	 */
	protected $acronym;
	
	/**
	 *
	 * @var string
	 */
	protected $type;
	
	/**
	 *
	 * @var integer
	 */
	protected $subject_id;
	
	/**
	 * Method to set the value of field id
	 *
	 * @param integer $id        	
	 * @return $this
	 */
	public function setId($id) {
		$this->id = $id;
		
		return $this;
	}
	
	/**
	 * Method to set the value of field acronym
	 *
	 * @param string $acronym        	
	 * @return $this
	 */
	public function setAcronym($acronym) {
		$this->acronym = $acronym;
		
		return $this;
	}
	
	/**
	 * Method to set the value of field type
	 *
	 * @param string $type        	
	 * @return $this
	 */
	public function setType($type) {
		$this->type = $type;
		
		return $this;
	}
	
	/**
	 * Method to set the value of field subject_id
	 *
	 * @param integer $subject_id        	
	 * @return $this
	 */
	public function setSubjectId($subject_id) {
		$this->subject_id = $subject_id;
		
		return $this;
	}
	
	/**
	 * Returns the value of field id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Returns the value of field acronym
	 *
	 * @return string
	 */
	public function getAcronym() {
		return $this->acronym;
	}
	
	/**
	 * Returns the value of field type
	 *
	 * @return string
	 */
	public function getType() {
		return $this->type;
	}
	
	/**
	 * Returns the value of field subject_id
	 *
	 * @return integer
	 */
	public function getSubjectId() {
		return $this->subject_id;
	}
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->hasMany ( 'id', 'Lessons', 'category_id', array (
				'alias' => 'Lessons' 
		) );
		$this->hasMany ( 'id', 'Questions', 'question_type', array (
				'alias' => 'Questions' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Categories[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Categories
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'categories';
	}
}
