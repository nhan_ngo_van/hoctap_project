<?php

namespace hoctap\Teacher\Models;

class ExamsQuestions extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var integer
	 */
	public $exam_id;
	
	/**
	 *
	 * @var integer
	 */
	public $question_id;
	
	/**
	 *
	 * @var integer
	 */
	public $question_number;
	
	/**
	 *
	 * @var string
	 */
	public $description;
	
	/**
	 *
	 * @var double
	 */
	public $score;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->belongsTo ( 'exam_id', 'hoctap\Teacher\Models\Exams', 'id', array (
				'alias' => 'Exams' 
		) );
		$this->belongsTo ( 'question_id', 'hoctap\Teacher\Models\Questions', 'id', array (
				'alias' => 'Questions' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ExamsQuestions[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ExamsQuestions
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'exams_questions';
	}
}
