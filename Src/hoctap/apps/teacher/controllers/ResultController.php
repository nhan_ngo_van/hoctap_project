<?php

namespace hoctap\Teacher\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\Exams;
use hoctap\Teacher\Models\ExamsQuestions;
use hoctap\Models\QuestionOfExam;
use hoctap\Models\ViewTestResult;
use hoctap\Models\ResultTest;
use hoctap\Models\ChartData;
use Phalcon\Mvc\Model\Query;
use hoctap\Models\ViewChart;

/**
 * Manage exam
 *
 * @author nhannv-pc
 *        
 */
const LinkExamForwardIndex = '/exam/index';
const LinkExamDirectIndex = '/teacher/exam/index';
const LinkExamDirectView = '/teacher/exam/view/';
class ResultController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->tag->prependTitle ( 'Kết quả thi ' );
		$this->assets->addJs ( 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js' );
	}
	/**
	 * Index action to manage question
	 */
	public function indexAction() {
		$numberPage = 2;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$exams = Exams::find ( array (
				'conditions' => 'author_id =?1',
				'bind' => array (
						1 => $this->session->get ( 'id' ) 
				) 
		) );
		if (count ( $exams ) == 0) {
			$this->flash->notice ( "Did not find any exams" );
		}
		$paginator = new Paginator ( array (
				'data' => $exams,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/exam/index/', $this->view->page );
	}
	
	/**
	 * View the result test
	 *
	 * @param unknown $id        	
	 */
	public function viewAction($id) {
		$this->viewChart ( $id );
		
		// exam id
		$this->view->exam_name = Exams::findFirstById ( $id )->name;
		
		// detail
		$results = ViewTestResult::find ( array (
				'conditions' => 'exam_id=?1 ',
				'bind' => array (
						1 => $id 
				),
				'order' => 'date_test desc' 
		) );
		$paginator = new Paginator ( array (
				'data' => $results,
				'limit' => 50,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
	}
	public function detailAction($id, $user) {
		$results = ViewTestResult::find ( array (
				'conditions' => 'user_id=?1 and exam_id=?2',
				'bind' => array (
						1 => $user,
						2 => $id 
				),
				'order' => 'date_test desc' 
		) );
		$this->view->results = $results;
		$this->view->info = '/teacher/result/reject/' . $id . '/' . $user;
	}
	public function rejectAction($id, $user) {
		$results = ResultTest::find ( array (
				'conditions' => 'user_id=?1 and exam_id=?2',
				'bind' => array (
						1 => $user,
						2 => $id 
				),
				'order' => 'date_test desc' 
		) );
		if ($results) {
			$results->delete ();
		}
		$this->response->redirect ( '/teacher/result/view/' . $id );
	}
	private function viewChart($id) {
		// initialize data for chart
		$viewChart = ViewChart::find ( array (
				'conditions' => 'exam_id=?1 ',
				'bind' => array (
						1 => $id 
				),
				'order' => 'choose_correct desc' 
		) );
		$data = null;
		$dataCover = null;
		$label = null;
		foreach ( $viewChart as $item ) {
			error_log ( $item->type );
			if ($item->choose_correct == true) {
				$data [count ( $data )] = $item->total;
				$label [count ( $label )] = $item->type;
				$dataCover [count ( $dataCover )] = $item->total;
			} else {
				
				if (! in_array ( $item->type, $label )) {
					$data [count ( $data )] = 0;
					$label [count ( $label )] = $item->type;
					$dataCover [count ( $dataCover )] = $item->total;
					error_log ( $item->total );
					error_log ( implode ( ' ', $dataCover ) );
				} else {
					$mixed = array_search ( $item->type, $label );
					$dataCover [$mixed] += $item->total;
				}
			}
			error_log ( implode ( ' ', $dataCover ) );
		}
		$this->view->data = $data;
		$this->view->label = $label;
		$this->view->dataCover = $dataCover;
	}
}

