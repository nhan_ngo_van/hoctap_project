<?php

namespace hoctap\Teacher\Controllers;

use hoctap\Base\Controllers\ControllerBase;

/**
 * Link to list question and manage question
 *
 * @var string
 */
const LinkQuestionIndex = '/teacher/question/index';
const LinkExamIndex = '/teacher/exam/index';
const LinkLessonIndex = '/teacher/lesson/index';
class IndexController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
	}
	public function indexAction() {
	}
}

