<?php

namespace hoctap\Teacher\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Questions;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\Exams;
use hoctap\Teacher\Models\ExamsQuestions;
use hoctap\Models\QuestionOfExam;
use hoctap\Forms\ExamForm;

/**
 * Manage exam
 *
 * @author nhannv-pc
 *        
 */
const LinkExamForwardIndex = '/exam/index';
const LinkExamDirectIndex = '/teacher/exam/index';
const LinkExamDirectView = '/teacher/exam/view/';
class ExamController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->tag->prependTitle ( 'Tạo đề thi ' );
		$this->assets->addJs ( '/public/js/editer/full/ckeditor.js' );
	}
	/**
	 * Index action to manage question
	 */
	public function indexAction() {
		$numberPage = 2;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$exams = Exams::find ( array (
				'conditions' => 'author_id =?1',
				'bind' => array (
						1 => $this->session->get ( 'id' ) 
				) 
		) );
		if (count ( $exams ) == 0) {
			$this->flash->notice ( "Did not find any exams" );
		}
		$paginator = new Paginator ( array (
				'data' => $exams,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/exam/index/', $this->view->page );
	}
	/**
	 * Create action to create new question
	 */
	public function createAction() {
		$this->tag->setTitle ( 'Tạo một đề thi mới' );
		if ($this->request->isPost ()) {
			
			$now = date ( 'Y-m-d H:i:s' );
			// create new exams from form
			$exam = new Exams ();
			$exam->name = $this->request->getPost ( 'name' );
			$exam->subject_id = $this->request->getPost ( 'subject_id' );
			$exam->description = $this->request->getPost ( 'description' );
			$exam->footer = $this->request->getPost ( 'footer' );
			$exam->author_id = $this->session->get ( 'id' );
			$exam->date_modify = $now;
			$exam->created_date = $now;
			
			// exam active view
			$activeView = $this->request->getPost ( 'active_exam_view' );
			$exam->active_view = $activeView == '' ? $now : $activeView;
			
			// answer active view
			$activeAnswerView = $this->request->getPost ( 'active_answer_view' );
			$exam->active_answer_view = $activeAnswerView == '' ? $now : $activeAnswerView;
			
			// active test until
			$activeTestUntil = $this->request->getPost ( 'test_until' );
			$exam->active_test_until = $activeTestUntil == '' ? $now : $activeTestUntil;
			$exam->limit_time = $this->request->getPost ( 'limit_time' );
			$exam->active_edit = 0;
			// get list question
			$numberOfQuestion = $this->request->getPost ( 'number_of_question' );
			if ($numberOfQuestion < 1) {
				$this->flash->error ( 'Không có câu hỏi nào được chọn' );
				return;
			}
			// get first question
			$questionID [0] = $this->request->getPost ( 'q' . '1' );
			// get other question
			for($i = 2; $i <= $numberOfQuestion; $i ++) {
				$questionID [$i - 1] = $this->request->getPost ( 'q' . $i );
			}
			
			// save exam
			if ($exam->save ()) {
				for($i = 0; $i < $numberOfQuestion; $i ++) {
					$q = new ExamsQuestions ();
					$q->question_id = $questionID [$i];
					$q->exam_id = $exam->id;
					$q->score = 1;
					$q->question_number = $i + 1;
					if (! $q->save ()) {
						foreach ( $q->getMessages () as $message ) {
							$this->flash->error ( 'Error:' . 'Xảy ra lỗi khi liên kết với câu hỏi' );
						}
					}
				}
			} else {
				$this->flash->error ( "Xảy ra lỗi khi tạo đề" );
			}
			return $this->response->redirect ( LinkExamDirectView . $exam->id );
		} else {
			$this->view->examForm = new ExamForm ();
		}
	}
	/**
	 * Form update question
	 *
	 * @param int $id
	 *        	:id of question
	 */
	public function updateAction($id) {
		$this->tag->setTitle ( 'Cập nhật đề thi' );
		if (! $this->request->isPost ()) {
			$exam = Exams::findFirstById ( $id );
			
			$questions = QuestionOfExam::find ( array (
					'conditions' => 'exam_id=?1',
					'bind' => array (
							1 => $exam->id 
					),
					'order' => 'question_number asc' 
			) );
			$this->view->questions = $questions;
			
			if (! $exam) {
				$this->flash->error ( 'Đề thi không tìm thấy' . $id );
			}
			if ($exam->author_id == $this->session->get ( 'id' )) {
				// view form
				$this->view->exam = $exam;
				$this->view->examForm = new ExamForm ( $exam, array (
						'edit' => true 
				) );
			} else {
				$this->flash->error ( 'Không có quyền truy cập' );
				$this->forward ( LinkQuestionIndex );
			}
		}
	}
	/**
	 * Delete exam by id
	 *
	 * @param int $id        	
	 */
	public function deleteAction($id) {
		// delete link question and exam
		$questionExam = ExamsQuestions::find ( array (
				'conditions' => 'exam_id = ?1',
				'bind' => array (
						1 => $id 
				) 
		) );
		foreach ( $questionExam as $qe ) {
			if (! $qe->delete ()) {
				$this->flash->error ( 'Gặp lỗi khi xóa' );
			}
		}
		$exam = Exams::findFirstById ( $id );
		if ($exam) {
			// validate permission only owner can delete
			if ($exam->author_id == $this->session->get ( 'id' )) {
				if (! $exam->delete ()) {
					// foreach ( $products->getMessages () as $message ) {
					// $this->flash->error ( $message );
					// }
					$this->flash->error ( 'Bạn không thể xóa đề thi đã có người thi' );
				} else {
					$this->flash->success ( 'Đã xóa: ' . $exam->name );
				}
			}
		} else {
			$this->flash->error ( 'Không tìm thấy đề thi' );
		}
		return $this->response->redirect ( LinkExamDirectIndex );
	}
	/**
	 * Save updated data
	 */
	public function saveAction() {
		if ($this->request->isPost ()) {
			if (! $this->request->hasPost ( 'id' )) {
				$this->flash->error ( 'Không có tham số' );
				return $this->forward ( LinkExamForwardIndex );
			}
			$id = $this->request->getPost ( 'id', 'int' );
			$exam = Exams::findFirstById ( $id );
			$exam->date_modify = date ( 'Y-m-d H:i:s' );
			
			if ($exam) {
				// exam information
				$exam->name = $this->request->getPost ( 'name' );
				$exam->subject_id = $this->request->getPost ( 'subject_id' );
				$exam->description = $this->request->getPost ( 'description' );
				$exam->footer = $this->request->getPost ( 'footer' );

				// check if set new data for active date time
				if ($this->request->has ( 'active_exam_view' ) && $this->request->getPost ( 'active_exam_view' ) != '') {
					$exam->active_view = $this->request->getPost ( 'active_exam_view' );
				}
				if ($this->request->has ( 'active_answer_view' ) && $this->request->getPost ( 'active_answer_view' ) != '') {
					$exam->active_answer_view = $this->request->getPost ( 'active_answer_view' );
				}
				
				// test until
				if ($this->request->has ( 'test_until' ) && $this->request->getPost ( 'test_until' ) != '') {
					$exam->active_test_until = $this->request->getPost ( 'test_until' );
				}
				// limit time
				$exam->limit_time = $this->request->getPost ( 'limit_time' );
				
				// author authenticate
				if ($exam->author_id == $this->session->get ( 'id' )) {
					if ($exam->save () == false) {
						foreach ( $exam->getMessages () as $message ) {
							$this->flash->error ( $message );
						}
						return $this->forward ( '/exam/update/' . $id );
					}
				}
				
				// question number
				$numberOfQuestion = $this->request->getPost ( 'number_of_question' );
				
				$exQuests = ExamsQuestions::find ( array (
						'conditions' => 'exam_id=?1',
						'bind' => array (
								1 => $exam->id 
						) 
				) );
				// delete question removed
				foreach ( $exQuests as $eq ) {
					$questID = $this->request->getPost ( 'question_' . $eq->id, 'int' );
					if ($questID) {
						if (Questions::findFirstById ( $questID )) {
							$eq->question_id = $questID;
							$eq->score = 1;
							$eq->save ();
						} else {
							$this->flash->error ( 'Không tìm thấy câu hỏi có ID=' . $questID );
							return $this->response->redirect ( LinkExamDirectView . $exam->id );
						}
					} else {
						$eq->delete ();
					}
				}
				
				$this->sortQuestion ( $exam->id );
				
				$oldLastQuest = count ( $exQuests );
				$exQuests = ExamsQuestions::find ( array (
						'conditions' => 'exam_id=?1',
						'bind' => array (
								1 => $exam->id 
						) 
				) );
				// new question
				$lastCurrentQuestion = count ( $exQuests );
				if ($oldLastQuest < $numberOfQuestion) {
					for($i = $numberOfQuestion; $i > $oldLastQuest; $i --) {
						$newQ = new ExamsQuestions ();
						$newQ->exam_id = $exam->id;
						$newQ->question_number = ++ $lastCurrentQuestion;
						if ($this->request->hasPost ( 'new_' . $i )) {
							$questionID = $this->request->getPost ( 'new_' . $i );
							if (Questions::findFirstById ( $questionID )) {
								$newQ->question_id = $questionID;
								$newQ->save ();
							} else {
								$this->flash->error ( 'Không tìm thấy câu hỏi có ID=' . $questionID );
								$this->response->redirect ( LinkExamDirectView . $exam->id );
							}
						}
					}
				}
				
				return $this->response->redirect ( LinkExamDirectView . $id );
			} else {
				$this->flash->error ( 'Không tìm thấy đề thi' );
				return $this->forward ( LinkExamForwardIndex );
			}
		} else {
			$this->flash->error ( 'Lỗi request' );
			return $this->forward ( LinkExamForwardIndex );
		}
		$this->flash->success ( 'Cập nhật thành công' );
	}
	/**
	 * View the question
	 *
	 * @param unknown $id        	
	 */
	public function viewAction($id) {
		if (! $this->request->isPost ()) {
			$exam = Exams::findFirstById ( $id );
			if ($exam) {
				$this->tag->setTitle ( $exam->name );
				$this->view->exam = $exam;
				$questions = QuestionOfExam::find ( array (
						'conditions' => 'exam_id=?1',
						'bind' => array (
								1 => $exam->id 
						),
						'order' => 'question_number asc' 
				) );
				$this->view->questions = $questions;
			} else {
				$this->flash->error ( 'Không tìm thấy đề thi' );
			}
		}
	}
	public function questionsAction() {
		$this->view->disable ();
		$response = new \Phalcon\Http\Response ();
		if ($this->request->has ( 'value' )) {
			$questionNumber = $this->request->get ( 'value' );
			$subject = $this->request->get ( 'subject' );
			$questions = Questions::find ( array (
					'conditions' => 'author_id =?1 and subject_id=?2',
					'bind' => array (
							1 => $this->session->get ( 'id' ),
							2 => $subject 
					) 
			) );
			$questionName = 'question_' . $questionNumber;
			
			$selectQuestion = '<input list="question' . $questionNumber . '" name="' . $questionName . '" placeholder="Nhập ID của câu hỏi" required>';
			$selectQuestion .= '<datalist id="question' . $questionNumber . '">';
			foreach ( $questions as $question ) {
				$question->question_name = htmlentities ( $question->question_name, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
				$selectQuestion .= '<option value="' . $question->id . '">' . $question->question_name . '</option>';
			}
			$selectQuestion .= '</datalist>';
			echo $selectQuestion;
		} else {
			echo 'None';
		}
	}
	public function questionForUpdateAction() {
		$this->view->disable ();
		$response = new \Phalcon\Http\Response ();
		if ($this->request->has ( 'value' )) {
			$questionNumber = $this->request->get ( 'value' );
			$subject = $this->request->get ( 'subject' );
			$questions = Questions::find ( array (
					'conditions' => 'author_id =?1 and subject_id=?2',
					'bind' => array (
							1 => $this->session->get ( 'id' ),
							2 => $subject 
					) 
			) );
			$questionName = 'new_' . $questionNumber;
			
			$selectQuestion = '<input list="question' . $questionNumber . '" name="' . $questionName . '" placeholder="Nhập ID của câu hỏi" required>';
			$selectQuestion .= '<datalist id="question' . $questionNumber . '">';
			foreach ( $questions as $question ) {
				$question->question_name = htmlentities ( $question->question_name, ENT_QUOTES | ENT_IGNORE, "UTF-8" );
				$selectQuestion .= '<option value="' . $question->id . '">' . $question->question_name . '</option>';
			}
			$selectQuestion .= '</datalist>';
			$selectQuestion .= '<i onclick="removeOption(' . $questionNumber . ')" class="glyphicon glyphicon-remove ' . $questionName . '" name="remove" id="remove_' . $question->id . '"></i><br/>';
			echo $selectQuestion;
		} else {
			echo 'None';
		}
	}
	/**
	 * Delete question in list and order question
	 *
	 * @param integer $id        	
	 */
	private function sortQuestion($examID) {
		$qe = ExamsQuestions::find ( array (
				'conditions' => 'exam_id=?1',
				'bind' => array (
						1 => $examID 
				),
				'order' => 'question_number asc' 
		) );
		
		$numberOfQuest = count ( $qe );
		if ($numberOfQuest > 0 && $qe [0]->question_number != 1) {
			$qe [0]->question_number = 1;
			$qe [0]->save ();
		}
		for($i = 0; $i < $numberOfQuest - 1; $i ++) {
			
			if ($qe [$i + 1]->question_number - $qe [$i]->question_number >= 0) {
				$qe [$i + 1]->question_number = $qe [$i]->question_number + 1;
				$qe [$i + 1]->save ();
			}
		}
	}
}

