<?php

namespace hoctap\Teacher\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Models\Lessons;
use hoctap\Forms\LessonForm;

const LinkLessonCreate = '/lesson/create';
const LinkLessonIndex = '/lesson/index/';
const ForwardLessonIndex = '/lesson/index';
const DirectLessonIndex = '/teacher' . ForwardLessonIndex;
class LessonController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->tag->prependTitle ( 'Bài Giảng - ' );
		$this->assets->addJs ( '/public/js/editer/full/ckeditor.js' );
	}
	/**
	 * Index action to manage lesson
	 */
	public function indexAction() {
		// handle request for paginator
		$numberPage = 1;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$lesson = Lessons::find ( array (
				'conditions' => 'author_id =?1',
				'bind' => array (
						1 => $this->session->get ( 'id' ) 
				) 
		) );
		if (count ( $lesson ) == 0) {
			$this->flash->notice ( "Did not find any lesson" );
		}
		$paginator = new Paginator ( array (
				'data' => $lesson,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/lesson/index/', $this->view->page );
	}
	/**
	 * Create action to create new lesson
	 */
	public function createAction() {
		$this->tag->setTitle ( 'Tạo mới bài giảng' );
		$lessonForm = new LessonForm ( null );
		if ($this->request->isPost ()) {
			
			$lesson = new Lessons ();
			$data = $this->request->getPost ();
			
			if (! $lessonForm->isValid ( $data, $lesson )) {
				foreach ( $lessonForm->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardLessonIndex );
			}
			
			// set author
			$lesson->author_id = $this->session->get ( 'id' );
			$lesson->date_modify = date ( 'Y-m-d H:i:s' );
			$lesson->created_date = date ( 'Y-m-d H:i:s' );
			if ($lesson->save () == false) {
				foreach ( $lesson->getMessages () as $message ) {
					$this->flash->error ( $message );
				}
				return $this->forward ( ForwardIndex );
			}
			$this->flash->success ( "Tạo thành công" );
			$this->response->redirect ( DirectLessonIndex );
		}
		$this->view->form = $lessonForm;
	}
	/**
	 * Form update lesson
	 *
	 * @param int $id
	 *        	:id of lesson
	 */
	public function updateAction($id) {
		$this->tag->setTitle ( 'Cập nhật bài giảng' );
		if (! $this->request->isPost ()) {
			$lesson = Lessons::findFirstById ( $id );
			if (! $lesson) {
				$this->flash->error ( 'Câu hỏi không tìm thấy' . $id );
			}
			if ($lesson->author_id == $this->session->get ( 'id' )) {
				// view form
				$this->view->lesson = $lesson;
				$this->view->form = new LessonForm ( $lesson, array (
						'edit' => true 
				) );
			} else {
				$this->flash->error ( 'Không có quyền truy cập' );
				$this->forward ( LinkLessonIndex );
			}
		}
	}
	/**
	 * Delete lesson by id
	 *
	 * @param int $id        	
	 */
	public function deleteAction($id) {
		$lesson = Lessons::findFirstById ( $id );
		if ($lesson) {
			// validate permission only owner can delete
			if ($lesson->author_id == $this->session->get ( 'id' )) {
				if (! $lesson->delete ()) {
					foreach ( $products->getMessages () as $message ) {
						$this->flash->error ( $message );
					}
				}
				$this->flash->success ( 'Đã xóa: ' . $lesson->name );
			}
		} else {
			$this->flash->error ( 'Không tìm thấy bài giảng' );
		}
		return $this->forward ( ForwardLessonIndex );
	}
	/**
	 * Save updated data
	 */
	public function saveAction() {
		if ($this->request->isPost ()) {
			if (! $this->request->hasPost ( 'id' )) {
				$this->flash->error ( 'Không có tham số' );
				return $this->forward ( ForwardLessonIndex );
			}
			$id = $this->request->getPost ( 'id', 'int' );
			$lesson = Lessons::findFirstById ( $id );
			$lesson->date_modify = date ( 'Y-m-d H:i:s' );
			if ($lesson) {
				
				$form = new LessonForm ();
				$this->view->form = $form;
				
				$data = $this->request->getPost ();
				
				// validate input form for model
				if ($form->isValid ( $data, $lesson )) {
					
					if ($lesson->author_id == $this->session->get ( 'id' )) {
						if ($lesson->save () == false) {
							foreach ( $lesson->getMessages () as $message ) {
								$this->flash->error ( $message );
							}
						}
						$this->flash->success ( 'Cập nhật thành công' );
						$this->response->redirect ( '/teacher/lesson/view/' . $id );
					}
					
					return $this->forward ( ForwardLessonIndex );
				} else {
					foreach ( $form->getMessages () as $message ) {
						$this->flash->error ( $message );
					}
					return $this->forward ( '/lesson/update/' . $id );
				}
			} else {
				$this->flash->error ( 'Không tìm thấy bài giảng' );
				return $this->forward ( ForwardLessonIndex );
			}
		} else {
			$this->flash->error ( 'Lỗi request' );
			return $this->forward ( ForwardLessonIndex );
		}
	}
	/**
	 * View the lesson
	 *
	 * @param unknown $id        	
	 */
	public function viewAction($id) {
		if (! $this->request->isPost ()) {
			$lesson = Lessons::findFirstById ( $id );
			if ($lesson) {
				$this->view->lesson = $lesson;
			} else {
				$this->flash->error ( 'Không tìm thấy bài giảng' );
			}
		}
	}
}

