<?php

namespace hoctap\Teacher\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Questions;
use hoctap\Forms\AnswerForm;
use hoctap\Models\Answers;
use Phalcon\Forms\Form;
use hoctap\Models\Exams;
use hoctap\Models\QuestionAnswer;

/**
 * Link to create answer
 *
 * @var string
 * @author nhannv-pc
 *        
 */
const LinkAnswerCreate = '/answer/create';
const LinkQuestionView = '/teacher/question/view/';
const LinkQuestionIndex = '/teacher/question/index/';
/**
 * Class create answer for question
 *
 * @author nhannv-pc
 *        
 */
class AnswerController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->tag->prependTitle ( 'Đáp án - ' );
		$this->assets->addJs ( '/public/js/editer/full/ckeditor.js' );
	}
	/**
	 * Create action to create new answer
	 */
	public function createAction($questionID) {
		$this->tag->setTitle ( 'Tạo đáp án' );
		
		if ($this->request->isPost ()) {
			
			$answer = new Answers ();
			
			$answer->answer_content = $this->request->getPost ( 'answer_content' );
			$answer->author_id = $this->session->get ( 'id' );
			$answer->date_modify = date ( 'Y-m-d H:i:s' );
			$answer->created_date = date ( 'Y-m-d H:i:s' );
			$answer->question_id = $this->request->getPost ( 'question_id' );
			
			if ($this->request->has ( 'active_view' ) && $this->request->getPost ( 'active_view' ) != '') {
				$active_view = $this->request->getPost ( 'active_view' );
			} else {
				$active_view = date ( 'Y-m-d H:i:s' );
			}
			$answer->active_view = $active_view;
			if ($answer->save ()) {
				$this->flash->success ( 'Tạo đáp án thành công' );
			} else {
				$this->flash->error ( 'Tạo đáp án không thành công' . $active_view );
			}
			
			$this->response->redirect ( LinkQuestionView . $answer->question_id );
		}
		$question = Questions::findFirstById ( $questionID );
		if ($question) {
			$this->view->question = $question;
		}
		$answerForm = new AnswerForm ( null, array (
				'question_id' => $question->id 
		) );
		$this->view->form = $answerForm;
		$this->view->currentTime = date ( 'Y-m-d h:i:s' );
	}
	/**
	 * Form update answer
	 *
	 * @param int $id
	 *        	:id of question
	 */
	public function updateAction($id) {
		$this->tag->setTitle ( 'Cập nhật đáp án' );
		
		$answer = Answers::findFirstById ( $id );
		if ($answer) {
			$question = Questions::findFirstById ( $answer->question_id );
			if ($question) {
				$this->view->question = $question;
			}
			$this->view->form = new AnswerForm ( $answer, array (
					'edit' => true,
					'question_id' => $answer->question_id 
			) );
		}
	}
	/**
	 * Display list answer of a exam
	 *
	 * @param integer $examID        	
	 */
	public function answersAction($examID) {
		if (! $this->request->isPost ()) {
			$exam = Exams::findFirstById ( $examID );
			if ($exam) {
				$this->tag->setTitle ( $exam->name );
				$this->view->exam = $exam;
				$questionAnswer = QuestionAnswer::find ( array (
						'conditions' => 'exam_id=?1',
						'bind' => array (
								1 => $exam->id 
						),
						'order' => 'question_number asc' 
				) );
				$this->view->questions = $questionAnswer;
			} else {
				$this->flash->error ( 'Không tìm thấy đề thi' );
			}
		}
	}
	/**
	 * Delete answer by id
	 *
	 * @param int $id        	
	 */
	public function deleteAction($id) {
		$answer = Answers::findFirstById ( $id );
		if ($answer) {
			// validate permission only owner can delete
			if ($answer->author_id == $this->session->get ( 'id' )) {
				if (! $answer->delete ()) {
					foreach ( $answer->getMessages () as $message ) {
						$this->flash->error ( $message );
					}
				}
				$this->flash->success ( 'Đã xóa đáp án ' );
			}
		} else {
			$this->flash->error ( 'Không tìm thấy đáp án' );
		}
		return $this->response->redirect ( LinkQuestionView . $answer->question_id );
	}
	/**
	 * Save updated data
	 */
	public function saveAction() {
		if (! $this->request->hasPost ( 'id' )) {
			$this->flash->error ( 'Không có tham số' );
			return $this->response->redirect ( LinkQuestionView . $answer->question_id );
		}
		
		$id = $this->request->getPost ( 'id', 'int' );
		$answer = Answers::findFirstById ( $id );
		$answer->date_modify = date ( 'Y-m-d H:i:s' );
		if ($this->request->hasPost ( 'active_view' ) && $this->request->getPost ( 'active_view' ) != '') {
			$answer->active_view = $this->request->getPost ( 'active_view' );
		}
		if ($this->request->isPost ()) {
			$form = new AnswerForm ( null );
			
			$data = $this->request->getPost ();
			if ($form->isValid ( $data, $answer )) {
				if ($answer->save ()) {
					$this->flash->success ( 'Cập nhật thành công' );
				} else {
					$this->flash->error ( 'Xảy ra lỗi' );
				}
			}
		}
		$this->response->redirect ( LinkQuestionView . $answer->question_id );
	}
}

