<?php

namespace hoctap\Teacher\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\Questions;
use hoctap\Forms\QuestionForm;
use Phalcon\Paginator\Adapter\Model as Paginator;
use hoctap\Forms\QuestionSearchForm;
use hoctap\Models\Answers;
use hoctap\Models\AnswersOfMulChoise;

/**
 * Link to create question
 *
 * @var string
 * @author nhannv-pc
 *        
 */
const LinkQuestionCreate = '/question/create/';
const LinkQuestionIndex = '/question/index/';
const LinkQuestionView = '/teacher/question/view/';
class QuestionController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->tag->prependTitle ( 'Câu hỏi - ' );
		$this->assets->addJs ( '/public/js/editer/full/ckeditor.js' );
		$this->assets->addJs ( '/public/js/teacher/question.js' );
	}
	/**
	 * Index action to manage question
	 */
	public function indexAction() {
		// handle request for paginator
		$this->view->searchQuestionForm = new QuestionSearchForm ();
		$numberPage = 2;
		if (! $this->request->isPost ()) {
			$numberPage = $this->request->getQuery ( "page", "int" );
		}
		// setup data for page
		$questions = Questions::find ( array (
				'conditions' => 'author_id =?1',
				'bind' => array (
						1 => $this->session->get ( 'id' ) 
				) 
		) );
		if (count ( $questions ) == 0) {
			$this->flash->notice ( "Did not find any questions" );
		}
		$paginator = new Paginator ( array (
				'data' => $questions,
				'limit' => 10,
				'page' => $numberPage 
		) );
		$this->view->page = $paginator->getPaginate ();
		$this->view->pageControl = $this->getControlPaginator ( '/teacher/question/index/', $this->view->page );
	}
	/**
	 * Create action to create new question
	 */
	public function createAction() {
		$this->tag->setTitle ( 'Tạo câu hỏi mới' );
		$questionForm = new QuestionForm ( null );
		if ($this->request->isPost ()) {
			if ($questionForm->isValid ( $this->request->getPost () ) != false) {
				$questionModel = new Questions ();
				// get value from form
				$questionModel->question_content = $this->request->getPost ( 'question_content' );
				$questionModel->question_name = $this->request->getPost ( 'question_name' );
				$questionModel->question_type = $this->request->getPost ( 'question_type' );
				$questionModel->subject_id = $this->request->getPost ( 'subject_id' );
				$questionModel->difficulty = $this->request->getPost ( 'difficulty' );
				$questionModel->active_view = $this->request->has ( 'active_view' ) ? $this->request->getPost ( 'active_view' ) : date ( 'Y-m-d h:i:s' );
				// get id of user
				$questionModel->author_id = $this->session->get ( 'id' );
				$questionModel->created_date = date ( 'Y-m-d H:i:s' );
				$questionModel->date_modify = date ( 'Y-m-d H:i:s' );
				$questionModel->mul_choise = $this->request->has ( 'true_answer' ) ? 1 : 0;
				if (! $questionModel->save ()) {
					$this->flash->error ( 'Tạo câu hỏi không thành công' );
					return $this->forward ( LinkQuestionIndex );
				} else {
					if ($questionModel->mul_choise == 1) {
						$choiseAnswer = new AnswersOfMulChoise ();
						
						$choiseAnswer->question_id = $questionModel->id;
						
						$choiseAnswer->option_a = $this->request->getPost ( 'a_answer' );
						$choiseAnswer->option_b = $this->request->getPost ( 'b_answer' );
						$choiseAnswer->option_c = $this->request->getPost ( 'c_answer' );
						$choiseAnswer->option_d = $this->request->getPost ( 'd_answer' );
						$choiseAnswer->option_e = $this->request->getPost ( 'e_answer' );
						$choiseAnswer->true_answer = $this->request->getPost ( 'true_answer' );
						$choiseAnswer->save ();
					}
					$this->flash->success ( 'Tạo câu hỏi thành công' );
					return $this->response->redirect ( '/teacher/question/view/' . $questionModel->id );
				}
			}
		}
		$this->view->form = $questionForm;
	}
	/**
	 * Form update question
	 *
	 * @param int $id
	 *        	:id of question
	 */
	public function updateAction($id) {
		$this->tag->setTitle ( 'Cập nhật câu hỏi' );
		if (! $this->request->isPost ()) {
			$question = Questions::findFirstById ( $id );
			if (! $question) {
				$this->flash->error ( 'Câu hỏi không tìm thấy' . $id );
			}
			if ($question->author_id == $this->session->get ( 'id' )) {
				// view form
				$this->view->question = $question;
				$this->view->form = new QuestionForm ( $question, array (
						'edit' => true 
				) );
				if ($question->mul_choise == 1) {
					$answerMultiChoise = AnswersOfMulChoise::find ( array (
							'conditions' => 'question_id=?1',
							'bind' => array (
									1 => $question->id 
							) 
					) );
					if (count ( $answerMultiChoise ) > 0) {
						$this->view->choise_answer = $answerMultiChoise [0];
					} else {
						$this->view->choise_answer = null;
					}
				} else {
					$this->view->choise_answer = null;
				}
			} else {
				$this->flash->error ( 'Không có quyền truy cập' );
				$this->forward ( LinkQuestionIndex );
			}
		}
	}
	/**
	 * Delete question by id
	 *
	 * @param int $id        	
	 */
	public function deleteAction($id) {
		$question = Questions::findFirstById ( $id );
		if ($question) {
			// validate permission only owner can delete
			if ($question->author_id == $this->session->get ( 'id' )) {
				if (! $question->delete ()) {
					foreach ( $products->getMessages () as $message ) {
						$this->flash->error ( $message );
					}
				}
				$this->flash->success ( 'Đã xóa: ' . $question->question_name );
			}
		} else {
			$this->flash->error ( 'Không tìm thấy câu hỏi' );
		}
		$this->forward ( LinkQuestionIndex );
	}
	/**
	 * Save updated data
	 */
	public function saveAction() {
		if ($this->request->isPost ()) {
			if (! $this->request->hasPost ( 'id' )) {
				$this->flash->error ( 'Không có tham số' );
				return $this->forward ( LinkQuestionIndex );
			}
			$id = $this->request->getPost ( 'id', 'int' );
			$question = Questions::findFirstById ( $id );
			$question->date_modify = date ( 'Y-m-d H:i:s' );
			if ($question) {
				
				$form = new QuestionForm ();
				$this->view->form = $form;
				
				$data = $this->request->getPost ();
				
				// validate input form for model
				if ($form->isValid ( $data, $question )) {
					
					// validate question content
					$valid = $this->containInvalid ( $question );
					if ($valid != false) {
						$this->flash->error ( "Câu hỏi chứa ký tự không được phép:" . $valid );
					} else {
						if ($question->author_id == $this->session->get ( 'id' )) {
							// update multi choise
							if (! $this->request->hasPost ( 'true_answer' )) {
								$question->mul_choise = 0;
							} else {
								$question->mul_choise = 1;
								$updateMulChoiseAnswer;
								// update multi choise
								$answerMultiChoise = AnswersOfMulChoise::find ( array (
										'conditions' => 'question_id=?1',
										'bind' => array (
												1 => $question->id 
										) 
								) );
								if (count ( $answerMultiChoise ) > 0) {
									$updateMulChoiseAnswer = $answerMultiChoise [0];
								} else {
									$updateMulChoiseAnswer = new AnswersOfMulChoise ();
									$updateMulChoiseAnswer->question_id = $question->id;
								}
								$updateMulChoiseAnswer->option_a = $this->request->getPost ( 'a_answer' );
								$updateMulChoiseAnswer->option_b = $this->request->getPost ( 'b_answer' );
								$updateMulChoiseAnswer->option_c = $this->request->getPost ( 'c_answer' );
								$updateMulChoiseAnswer->option_d = $this->request->getPost ( 'd_answer' );
								$updateMulChoiseAnswer->option_e = $this->request->getPost ( 'e_answer' );
								$updateMulChoiseAnswer->true_answer = $this->request->getPost ( 'true_answer' );
								$updateMulChoiseAnswer->save ();
							}
							// save question
							if ($question->save () == false) {
								foreach ( $question->getMessages () as $message ) {
									$this->flash->error ( $message );
								}
								return $this->forward ( '/question/update/' . $id );
							}
							
							$this->flash->success ( 'Cập nhật thành công' );
						}
					}
					
					return $this->response->redirect ( LinkQuestionView . $id );
				} else {
					foreach ( $form->getMessages () as $message ) {
						$this->flash->error ( $message );
					}
					return $this->forward ( '/question/update/' . $id );
				}
			} else {
				$this->flash->error ( 'Không tìm thấy câu hỏi' );
				return $this->forward ( LinkQuestionIndex );
			}
		} else {
			$this->flash->error ( 'Lỗi request' );
			return $this->forward ( LinkQuestionIndex );
		}
	}
	
	/**
	 * View the question
	 *
	 * @param unknown $id        	
	 */
	public function viewAction($id) {
		if (! $this->request->isPost ()) {
			$question = Questions::findFirstById ( $id );
			if ($question) {
				$this->tag->setTitle ( $question->question_name );
				$this->view->question = $question;
				$answers = Answers::find ( array (
						'conditions' => 'question_id=?1',
						'bind' => array (
								1 => $question->id 
						) 
				) );
				if ($answers) {
					$this->view->answers = $answers;
				} else {
					$this->view->answers = null;
				}
				// multi choise
				$multiChoise = AnswersOfMulChoise::find ( array (
						'conditions' => 'question_id=?1',
						'bind' => array (
								1 => $question->id 
						) 
				) );
				if (count ( $multiChoise ) > 0) {
					$this->view->multiChoise = $multiChoise [0];
				} else {
					$this->view->multiChoise = null;
				}
			} else {
				$this->flash->error ( 'Không tìm thấy câu hỏi' );
			}
		}
	}
	/**
	 * validate question
	 *
	 * @param Questions $question        	
	 * @return false : invalid
	 * @return true : valid
	 */
	private function containInvalid(Questions $question) {
		$invalid = parent::containInvalidChar ( $question->question_name );
		if ($invalid) {
			return $invalid;
		}
		$invalid = parent::containInvalidChar ( $question->question_content );
		if ($invalid) {
			return $invalid;
		}
		return false;
	}
}

