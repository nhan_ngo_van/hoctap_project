<?php

namespace hoctap\Student\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Models\ViewTestResult;
use hoctap\Models\Exams;
use hoctap\Models\ViewChart;
use hoctap\Models\UserViewChart;

class ResultController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->assets->addJs ( 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.js' );
	}
	public function indexAction() {
		$results = ViewTestResult::find ( array (
				'conditions' => 'user_id=?1',
				'bind' => array (
						1 => $this->session->get ( 'id' ) 
				),
				'order' => 'date_test desc' 
		) );
		$this->view->tests = $results;
		
		// chart data
		// initialize data for chart
		$viewChart = UserViewChart::find ( array (
				'conditions' => 'user_id=?2',
				'bind' => array (
						2 => $this->session->get ( 'id' ) 
				),
				'order' => 'choose_correct desc' 
		) );
		$chartData;
		$data;
		$label;
		foreach ( $viewChart as $item ) {
			if ($item->choose_correct == true) {
				$data [count ( $data )] = $item->total;
				$label [count ( $label )] = $item->type;
			} else {
				if (! in_array ( $item->type, $label )) {
					$data [count ( $data )] = 0;
					$label [count ( $label )] = $item->type;
				}
			}
		}
		$this->view->data = $data;
		$this->view->label = $label;
	}
	public function examAction($id) {
		$this->viewChart ( $id );
		// data
		$exam = Exams::findFirstById ( $id );
		if ($exam->active_answer_view < date ( 'Y-m-d H:i:s' )) {
			$this->view->showAnswer = true;
		} else {
			$this->view->showAnswer = false;
		}
		$results = ViewTestResult::find ( array (
				'conditions' => 'user_id=?1 and exam_id=?2',
				'bind' => array (
						1 => $this->session->get ( 'id' ),
						2 => $id 
				),
				'order' => 'date_test desc' 
		) );
		$this->view->results = $results;
	}
	private function viewChart($id) {
		// initialize data for chart
		$viewChart = UserViewChart::find ( array (
				'conditions' => 'exam_id=?1 and user_id=?2',
				'bind' => array (
						1 => $id,
						2 => $this->session->get ( 'id' ) 
				),
				'order' => 'choose_correct desc' 
		) );
		$data;
		$label;
		$dataCover;
		foreach ( $viewChart as $item ) {
			if ($item->choose_correct == true) {
				$data [count ( $data )] = $item->total;
				$label [count ( $label )] = $item->type;
				$dataCover [count ( $dataCover )] = $item->total;
			} else {
				
				if (! in_array ( $item->type, $label )) {
					$data [count ( $data )] = 0;
					$label [count ( $label )] = $item->type;
					$dataCover [count ( $dataCover )] = $item->total;
				} else {
					$mixed = array_search ( $item->type, $label );
					$dataCover [$mixed] += $item->total;
				}
			}
		}
		$this->view->data = $data;
		$this->view->label = $label;
		$this->view->coverData = $dataCover;
	}
}

