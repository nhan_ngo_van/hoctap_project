<?php

namespace hoctap\Student\Controllers;

use hoctap\Base\Controllers\ControllerBase;
use hoctap\Forms\RequestForm;
use hoctap\Models\TRequest;

class IndexController extends ControllerBase {
	public function initialize() {
		parent::initialize ();
		$this->assets->addJs ( '/public/js/editer/basic/ckeditor.js' );
	}
	public function indexAction() {
	}
	public function lessonsAction() {
	}
	public function examsAction() {
	}
	public function questionsAction() {
	}
	/**
	 * request to become teacher
	 */
	public function changeAction() {
		if (! $this->request->isPost ()) {
			$requestForm = new RequestForm ();
			$this->view->form = $requestForm;
		} else {
			$id = $this->session->get ( 'id' );
			
			$requestModel = TRequest::findFirstById ( $id );
			if (! $checkRequest) {
				$requestModel = new TRequest ();
				
				$requestModel->id = $id;
			}
			$requestModel->create_date = date ( 'Y-m-d H:i:s' );
			$requestModel->title = $this->request->getPost ( 'title' );
			$requestModel->content = $this->request->getPost ( 'content' );
			
			if ($requestModel->save ()) {
				$this->flash->success ( 'Đăng ký thành công, Vui lòng đợi phản hồi' );
			} else {
				$this->flash->error ( 'Xảy ra lỗi khi đăng ký, vui lòng thử lại sau' );
			}
			$this->response->redirect ( '/student/index/' );
		}
	}
}

