<?php

namespace hoctap\Models;

class Topics extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var string
	 */
	public $content;
	
	/**
	 *
	 * @var string
	 */
	public $create_date;
	
	/**
	 *
	 * @var string
	 */
	public $date_modify;
	
	/**
	 *
	 * @var integer
	 */
	public $author_id;
	
	/**
	 *
	 * @var integer
	 */
	public $category_id;
	
	/**
	 *
	 * @var integer
	 */
	public $rate;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->hasMany ( 'id', 'hoctap\Models\AnswerTopic', 'topic_id', array (
				'alias' => 'AnswerTopic' 
		) );
		$this->belongsTo ( 'author_id', 'hoctap\Models\Users', 'id', array (
				'alias' => 'Users' 
		) );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'topics';
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Topics[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Topics
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
}
