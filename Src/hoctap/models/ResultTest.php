<?php

namespace hoctap\Models;

class ResultTest extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var integer
	 */
	public $exam_id;
	
	/**
	 *
	 * @var integer
	 */
	public $user_id;
	
	/**
	 *
	 * @var string
	 */
	public $choose_answer;
	
	/**
	 *
	 * @var string
	 */
	public $score;
	
	/**
	 *
	 * @var string
	 */
	public $other;
	
	/**
	 *
	 * @var string
	 */
	public $date_test;
	
	/**
	 *
	 * @var string
	 */
	public $result_testcol;
	
	/**
	 *
	 * @var integer
	 */
	public $again_test;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->belongsTo ( 'exam_id', 'hoctap\Models\Exams', 'id', array (
				'alias' => 'Exams' 
		) );
		$this->belongsTo ( 'user_id', 'hoctap\Models\Users', 'id', array (
				'alias' => 'Users' 
		) );
		$this->belongsTo ( 'question_id', 'hoctap\Models\Questions', 'id', array (
				'alias' => 'Questions' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ResultTest[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ResultTest
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'result_test';
	}
}
