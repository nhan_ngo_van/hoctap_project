<?php

namespace hoctap\Models;

class QuestionOfExam extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var integer
	 */
	public $exam_id;
	
	/**
	 *
	 * @var integer
	 */
	public $question_number;
	
	/**
	 *
	 * @var string
	 */
	public $question_desc;
	
	/**
	 *
	 * @var integer
	 */
	public $question_id;
	
	/**
	 *
	 * @var string
	 */
	public $question_name;
	
	/**
	 *
	 * @var string
	 */
	public $question_content;
	
	/**
	 *
	 * @var integer
	 */
	public $question_type;
	
	/**
	 *
	 * @var string
	 */
	public $subject;
	
	/**
	 *
	 * @var string
	 */
	public $option_a;
	
	/**
	 *
	 * @var string
	 */
	public $option_b;
	
	/**
	 *
	 * @var string
	 */
	public $option_c;
	
	/**
	 *
	 * @var string
	 */
	public $option_d;
	
	/**
	 *
	 * @var string
	 */
	public $option_e;
	
	/**
	 *
	 * @var string
	 */
	public $true_answer;
	
	/**
	 *
	 * @var double
	 */
	public $score;
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return QuestionOfExam[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return QuestionOfExam
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'question_of_exam';
	}
}
