<?php

namespace hoctap\Models;

class ChartData extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var integer
	 */
	public $user_id;
	
	/**
	 *
	 * @var integer
	 */
	public $category_id;
	
	/**
	 *
	 * @var integer
	 */
	public $subject_id;
	
	/**
	 *
	 * @var integer
	 */
	public $exam_id;
	
	/**
	 *
	 * @var integer
	 */
	public $choose_correct;
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ChartData[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ChartData
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'chart_data';
	}
}
