<?php

namespace hoctap\Models;

class Answers extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $created_date;
	
	/**
	 *
	 * @var string
	 */
	public $date_modify;
	
	/**
	 *
	 * @var string
	 */
	public $answer_content;
	
	/**
	 *
	 * @var integer
	 */
	public $author_id;
	
	/**
	 *
	 * @var integer
	 */
	public $question_id;
	
	/**
	 *
	 * @var \DateTime
	 */
	public $active_view;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->hasMany ( 'id', 'hoctap\Models\QuestionsAnswers', 'answers_id', array (
				'alias' => 'QuestionsAnswers' 
		) );
		$this->belongsTo ( 'author_id', 'hoctap\Models\Users', 'id', array (
				'alias' => 'Users' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Answers[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Answers
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'answers';
	}
}
