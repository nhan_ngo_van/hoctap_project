<?php

namespace hoctap\Models;

class CommentsOfLessons extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $content;
	
	/**
	 *
	 * @var integer
	 */
	public $author_id;
	
	/**
	 *
	 * @var string
	 */
	public $date_modify;
	
	/**
	 *
	 * @var integer
	 */
	public $lesson_id;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->belongsTo ( 'author_id', 'hoctap\Models\Users', 'id', array (
				'alias' => 'Users' 
		) );
		$this->belongsTo ( 'lesson_id', 'hoctap\Models\Lessons', 'id', array (
				'alias' => 'Lessons' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return CommentsOfLessons[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return CommentsOfLessons
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'comments_of_lessons';
	}
}
