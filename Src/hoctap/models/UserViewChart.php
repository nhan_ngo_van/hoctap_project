<?php

namespace hoctap\Models;

class UserViewChart extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var string
	 */
	public $type;
	
	/**
	 *
	 * @var integer
	 */
	public $choose_correct;
	
	/**
	 *
	 * @var integer
	 */
	public $user_id;
	
	/**
	 *
	 * @var integer
	 */
	public $exam_id;
	
	/**
	 *
	 * @var string
	 */
	public $total;
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'user_view_chart';
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return UserViewChart[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return UserViewChart
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
}
