<?php

namespace hoctap\Models;

class Exams extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var integer
	 */
	public $subject_id;
	
	/**
	 *
	 * @var string
	 */
	public $description;
	
	/**
	 *
	 * @var string
	 */
	public $created_date;
	
	/**
	 *
	 * @var string
	 */
	public $date_modify;
	
	/**
	 *
	 * @var integer
	 */
	public $author_id;
	
	/**
	 *
	 * @var string
	 */
	public $footer;
	
	/**
	 *
	 * @var string
	 */
	public $active_view;
	
	/**
	 *
	 * @var string
	 */
	public $active_answer_view;
	
	/**
	 *
	 * @var integer
	 */
	public $active_edit;
	
	/**
	 *
	 * @var string
	 */
	public $active_test_until;
	
	/**
	 *
	 * @var integer
	 */
	public $limit_time;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->hasMany ( 'id', 'hoctap\Models\CommentsOfExams', 'exam_id', array (
				'alias' => 'CommentsOfExams' 
		) );
		$this->hasMany ( 'id', 'hoctap\Models\ExamsQuestions', 'exam_id', array (
				'alias' => 'ExamsQuestions' 
		) );
		$this->belongsTo ( 'author_id', 'hoctap\Models\Users', 'id', array (
				'alias' => 'Users' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Exams[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Exams
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'exams';
	}
}
