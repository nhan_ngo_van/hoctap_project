<?php

namespace hoctap\Models;

class QuestionAnswer extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var integer
	 */
	public $exam_id;
	
	/**
	 *
	 * @var integer
	 */
	public $question_number;
	
	/**
	 *
	 * @var string
	 */
	public $question_desc;
	
	/**
	 *
	 * @var integer
	 */
	public $question_id;
	
	/**
	 *
	 * @var string
	 */
	public $question_name;
	
	/**
	 *
	 * @var string
	 */
	public $question_content;
	
	/**
	 *
	 * @var integer
	 */
	public $question_type;
	
	/**
	 *
	 * @var integer
	 */
	public $difficulty;
	
	/**
	 *
	 * @var string
	 */
	public $answer_content;
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return QuestionAnswer[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return QuestionAnswer
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'question_answer';
	}
}
