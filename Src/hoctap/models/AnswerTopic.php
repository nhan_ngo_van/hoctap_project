<?php

namespace hoctap\Models;

class AnswerTopic extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $content;
	
	/**
	 *
	 * @var integer
	 */
	public $author_id;
	
	/**
	 *
	 * @var integer
	 */
	public $topic_id;
	
	/**
	 *
	 * @var integer
	 */
	public $rate;
	/**
	 *
	 * @var string
	 */
	public $date_modify;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->belongsTo ( 'author_id', 'hoctap\Models\Users', 'id', array (
				'alias' => 'Users' 
		) );
		$this->belongsTo ( 'topic_id', 'hoctap\Models\Topics', 'id', array (
				'alias' => 'Topics' 
		) );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'answer_topic';
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return AnswerTopic[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return AnswerTopic
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
}
