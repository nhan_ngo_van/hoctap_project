<?php

namespace hoctap\Models;

class ViewTopicAnswer extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $content;
	
	/**
	 *
	 * @var integer
	 */
	public $author_id;
	
	/**
	 *
	 * @var integer
	 */
	public $topic_id;
	
	/**
	 *
	 * @var integer
	 */
	public $rate;
	
	/**
	 *
	 * @var string
	 */
	public $date_modify;
	
	/**
	 *
	 * @var string
	 */
	public $first_name;
	
	/**
	 *
	 * @var string
	 */
	public $facebook;
	
	/**
	 *
	 * @var string
	 */
	public $icon_url;
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'view_topic_answer';
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ViewTopicAnswer[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ViewTopicAnswer
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
}
