<?php

namespace hoctap\Models;

class ViewChart extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var string
	 */
	public $type;
	
	/**
	 *
	 * @var integer
	 */
	public $choose_correct;
	
	/**
	 *
	 * @var string
	 */
	public $total;
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'view_chart';
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ViewChart[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ViewChart
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
}
