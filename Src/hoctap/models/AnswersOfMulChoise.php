<?php

namespace hoctap\Models;

class AnswersOfMulChoise extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var integer
	 */
	public $question_id;
	
	/**
	 *
	 * @var string
	 */
	public $option_a;
	
	/**
	 *
	 * @var string
	 */
	public $option_b;
	
	/**
	 *
	 * @var string
	 */
	public $option_c;
	
	/**
	 *
	 * @var string
	 */
	public $option_d;
	
	/**
	 *
	 * @var string
	 */
	public $option_e;
	
	/**
	 *
	 * @var string
	 */
	public $true_answer;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->belongsTo ( 'question_id', 'hoctap\Models\Questions', 'id', array (
				'alias' => 'Questions' 
		) );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'answers_of_mul_choise';
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return AnswersOfMulChoise[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return AnswersOfMulChoise
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
}
