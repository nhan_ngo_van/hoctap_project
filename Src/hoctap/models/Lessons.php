<?php

namespace hoctap\Models;

class Lessons extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var string
	 */
	public $content;
	
	/**
	 *
	 * @var integer
	 */
	public $category_id;
	
	/**
	 *
	 * @var integer
	 */
	public $subject_id;
	
	/**
	 *
	 * @var integer
	 */
	public $author_id;
	
	/**
	 *
	 * @var string
	 */
	public $created_date;
	
	/**
	 *
	 * @var string
	 */
	public $date_modify;
	
	/**
	 *
	 * @var string
	 */
	public $active_view;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->hasMany ( 'id', 'hoctap\Models\CommentsOfLessons', 'lesson_id', array (
				'alias' => 'CommentsOfLessons' 
		) );
		$this->hasMany ( 'id', 'hoctap\Models\CommentsOfQuestions', 'question_id', array (
				'alias' => 'CommentsOfQuestions' 
		) );
		$this->belongsTo ( 'category_id', 'hoctap\Models\Categories', 'id', array (
				'alias' => 'Categories' 
		) );
		$this->belongsTo ( 'subject_id', 'hoctap\Models\Subjects', 'id', array (
				'alias' => 'Subjects' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Lessons[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Lessons
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'lessons';
	}
}
