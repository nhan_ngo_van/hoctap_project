<?php

namespace hoctap\Models;

class ViewTestResult extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var integer
	 */
	public $exam_id;
	
	/**
	 *
	 * @var integer
	 */
	public $user_id;
	
	/**
	 *
	 * @var string
	 */
	public $choose_answer;
	
	/**
	 *
	 * @var string
	 */
	public $score;
	
	/**
	 *
	 * @var string
	 */
	public $other;
	
	/**
	 *
	 * @var string
	 */
	public $date_test;
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'view_test_result';
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ViewTestResult[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return ViewTestResult
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
}
