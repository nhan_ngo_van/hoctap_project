<?php

namespace hoctap\Models;

class Subjects extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	protected $id;
	
	/**
	 *
	 * @var string
	 */
	protected $acronym;
	
	/**
	 *
	 * @var string
	 */
	protected $name;
	
	/**
	 * Method to set the value of field id
	 *
	 * @param integer $id        	
	 * @return $this
	 */
	public function setId($id) {
		$this->id = $id;
		
		return $this;
	}
	
	/**
	 * Method to set the value of field acronym
	 *
	 * @param string $acronym        	
	 * @return $this
	 */
	public function setAcronym($acronym) {
		$this->acronym = $acronym;
		
		return $this;
	}
	
	/**
	 * Method to set the value of field name
	 *
	 * @param string $name        	
	 * @return $this
	 */
	public function setName($name) {
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * Returns the value of field id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * Returns the value of field acronym
	 *
	 * @return string
	 */
	public function getAcronym() {
		return $this->acronym;
	}
	
	/**
	 * Returns the value of field name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->hasMany ( 'id', 'Lessons', 'subject_id', array (
				'alias' => 'Lessons' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Subjects[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Subjects
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'subjects';
	}
}
