<?php

namespace hoctap\Models;

class Books extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var string
	 */
	public $description;
	
	/**
	 *
	 * @var integer
	 */
	public $lesson_id;
	
	/**
	 *
	 * @var integer
	 */
	public $page_number;
	
	/**
	 *
	 * @var integer
	 */
	public $author_id;
	
	/**
	 *
	 * @var string
	 */
	public $created_date;
	
	/**
	 *
	 * @var string
	 */
	public $date_modify;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->belongsTo ( 'lesson_id', 'hoctap\Models\Lessons', 'id', array (
				'alias' => 'Lessons' 
		) );
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Books[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return Books
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'books';
	}
}
