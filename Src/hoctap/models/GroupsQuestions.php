<?php

namespace hoctap\Models;

class GroupsQuestions extends \Phalcon\Mvc\Model {
	
	/**
	 *
	 * @var integer
	 */
	public $id;
	
	/**
	 *
	 * @var string
	 */
	public $name;
	
	/**
	 *
	 * @var string
	 */
	public $description;
	
	/**
	 *
	 * @var integer
	 */
	public $question_id;
	
	/**
	 *
	 * @var integer
	 */
	public $number_order;
	
	/**
	 *
	 * @var integer
	 */
	public $author_id;
	
	/**
	 *
	 * @var string
	 */
	public $reated_date;
	
	/**
	 *
	 * @var string
	 */
	public $date_modify;
	
	/**
	 * Initialize method for model.
	 */
	public function initialize() {
		$this->belongsTo ( 'question_id', 'hoctap\Models\Questions', 'id', array (
				'alias' => 'Questions' 
		) );
		$this->belongsTo ( 'author_id', 'hoctap\Models\Users', 'id', array (
				'alias' => 'Users' 
		) );
	}
	
	/**
	 * Returns table name mapped in the model.
	 *
	 * @return string
	 */
	public function getSource() {
		return 'groups_questions';
	}
	
	/**
	 * Allows to query a set of records that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return GroupsQuestions[]
	 */
	public static function find($parameters = null) {
		return parent::find ( $parameters );
	}
	
	/**
	 * Allows to query the first record that match the specified conditions
	 *
	 * @param mixed $parameters        	
	 * @return GroupsQuestions
	 */
	public static function findFirst($parameters = null) {
		return parent::findFirst ( $parameters );
	}
}
