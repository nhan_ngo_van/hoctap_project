/*
 * post comment and reload this page
 */
function postComment(kind, id) {
	var xmlHttp = new XMLHttpRequest();
	var message = editor.getData();

	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
			console.log(xmlHttp.responseText);
			window.location.href = document.URL;
		}
	};
	message = encodeURIComponent(message);
	console.log(message);
	url = "/anonymous/index/comment?kind=" + kind + "&id=" + id + "&content="
			+ message;
	xmlHttp.open("GET", url, true);
	xmlHttp.send();
}
//$(document).ready(function(){
//    $('[data-toggle="tooltip"]').tooltip();   
//});