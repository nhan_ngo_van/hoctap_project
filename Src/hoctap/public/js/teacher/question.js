var clickedAddChoiseQuestion = false;
function addMultiChoise() {
	if (!clickedAddChoiseQuestion) {
		clickedAddChoiseQuestion = true;
		$("#add-multi-choise").attr('value', 'Hủy đáp án trắc nghiệm');
		$("#answer_list_choose").append(
				"<label class='control-label col-md-2'>Đáp án A </label>");
		$("#answer_list_choose")
				.append(
						"<div class='col-md-10'><input type='text' name='a_answer' class='form-control'></div>");

		$("#answer_list_choose").append(
				"<label class='control-label col-md-2'>Đáp án B </label>");
		$("#answer_list_choose")
				.append(
						"<div class='col-md-10'><input type='text' name='b_answer' class='form-control'></div>");

		$("#answer_list_choose").append(
				"<label class='control-label col-md-2'>Đáp án C</label>");
		$("#answer_list_choose")
				.append(
						"<div class='col-md-10'><input type='text' name='c_answer' class='form-control'></div>");

		$("#answer_list_choose").append(
				"<label class='control-label col-md-2'>Đáp án D</label>");
		$("#answer_list_choose")
				.append(
						"<div class='col-md-10'><input type='text' name='d_answer' class='form-control'></div>");

		$("#answer_list_choose").append(
				"<label class='control-label col-md-2'>Đáp án E</label>");
		$("#answer_list_choose")
				.append(
						"<div class='col-md-10'><input type='text' name='e_answer' class='form-control'></div>");

		$("#answer_list_choose")
				.append(
						"<label class='control-label col-md-2 mb-5'>Đáp án đúng</label>");
		$("#answer_list_choose")
				.append(
						"<div class='col-md-10 mb-5'><input type='text' name='true_answer' class='form-control'></div>");
	} else {
		$("#answer_list_choose").html('');
		clickedAddChoiseQuestion = false;
		$("#add-multi-choise").attr('value', 'Đáp án trắc nghiệm');
	}
}