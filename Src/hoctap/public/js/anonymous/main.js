
function startTest() {
	var minute = checkTime(Math.floor(totalTime / 60000));
	var seconds = checkTime(Math.floor(totalTime % 60000) / 1000);
	document.getElementById('timer').innerHTML = minute + ":" + seconds;
	var t = setTimeout(startTest, 1000);
	totalTime -= 1000;
	if (totalTime <= 0) {
		document.getElementById("test_form").submit();
	}
}
function checkTime(i) {
	if (i < 10) {
		i = "0" + i
	}
	return i;
}