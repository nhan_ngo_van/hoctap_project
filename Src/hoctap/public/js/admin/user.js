$(document).ready(function() {
	var id = {
		"user-id" : $("#user-id").text()
	};

	if (id !== null) {
		$('#accept-teacher').click(function() {
			$.post('/admin/ajax/acceptRequest/', id, acceptCallback);
		});
		$('#deny-teacher').click(function() {
			$.post('/admin/ajax/denyRequest/', id, denyCallback);
		});
	}

});
function acceptCallback(res) {
	console.log("accepted:" + res);
	window.location.replace(res);
}
function denyCallback(res) {
	console.log("denied" + res);
	window.location.replace(res);
}