drop database luyentai_luyentai;
create database luyentai_luyentai DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
create user 'luyentai_hoctap'@'localhost' identified by 'Tudo2509';
GRANT ALL PRIVILEGES ON luyentai_luyentai.* TO 'luyentai_hoctap'@'localhost';
use luyentai_luyentai;

create table user_types(
	id int primary key not null auto_increment,
	module varchar(20) not null,
	name varchar(100) not null
);
create table subjects(
	id int primary key not null auto_increment,
	acronym varchar(20) not null,
	name varchar(200) not null
);
create table users(
	 id int primary key not null auto_increment,
	 first_name varchar(20)  not null,
	 user_name varchar(100),
	 password varchar(100),
	 last_name varchar(40),
	 mobile_phone varchar(15),
	 bithday date,
	 email varchar(100),
	 facebook varchar(200),
	 address varchar(200),
	 school varchar(200),
	 education_level int,
	 rank int,
	 join_date date not null,
	 user_type int not null,
	 active boolean not null default false,
	 icon_url varchar(500),
	 day_block int,
	 foreign key (user_type) references user_types(id)
	 );

create table categories(
	id int primary key not null auto_increment,
	acronym varchar(20),
	type varchar(100),
	subject_id int not null,
	foreign key (subject_id) references subjects(id) on update CASCADE
 );

create table questions(
	id int primary key not null auto_increment,
	question_name varchar(100),
	question_content varchar(4000),
	question_type int not null,
	difficulty int,
	author_id int not null,
	created_date datetime not null,
	date_modify datetime,
	subject_id int not null,
	foreign key (question_type) references categories(id), 
	foreign key (subject_id) references subjects(id), 
	foreign key (author_id) references users(id)
 );

create table exams(
	id int primary key not null auto_increment,
	name varchar (200) not null,
	description varchar(2000),
	created_date datetime not null,
	date_modify datetime,
	author_id int not null,
	footer varchar(300),
	foreign key (author_id) references users(id)
 );

create table exams_questions(
	id int auto_increment not null primary key,
	exam_id int not null,
	question_id int not null,
	question_number int not null,
	description varchar(300),
	foreign key (exam_id) references exams(id) on delete CASCADE on update CASCADE,
	foreign key (question_id) references questions(id) on delete CASCADE on update CASCADE
);

create table answers(
	id int not null primary key auto_increment,
	created_date datetime not null,
	date_modify datetime not null,
	answer_content varchar(5000) not null,
	author_id int not null,
	question_id int not null,
	active_view int not null default 1,
	foreign key (question_id) references questions(id) on delete CASCADE on update CASCADE,
	foreign key (author_id) references users(id)
);

create table answers_of_mul_choise(
	id int not null primary key auto_increment,
	question_id int not null,
	option_a varchar(500),
	option_b varchar(500),
	option_c varchar(500),
	option_d varchar(500),
	option_e varchar(500),
	true_answer varchar(500),
	foreign key (question_id) references questions(id)
);
create table lessons(
	id int not null primary key auto_increment,
	name varchar(200) not null,
	content mediumtext not null,
	category_id int not null,
	subject_id int not null,
	author_id int not null,
	created_date datetime not null,
	date_modify datetime not null,
	foreign key (category_id) references categories(id),
	foreign key (subject_id) references subjects(id),
	foreign key (author_id) references users(id)
);
create table comments_of_exams(
	id int not null primary key auto_increment,
	content varchar(1000) not null,
	author_id int not null,
	date_modify datetime not null,
	exam_id int not null,
	foreign key (author_id) references users(id) on delete CASCADE on update CASCADE,
	foreign key (exam_id) references exams(id) on delete CASCADE on update CASCADE
);
create table comments_of_lessons(
	id int not null primary key auto_increment,
	content varchar(1000) not null,
	author_id int not null,
	date_modify datetime not null,
	lesson_id int not null,
	foreign key (author_id) references users(id) on delete CASCADE on update CASCADE,
	foreign key (lesson_id) references lessons(id) on delete CASCADE on update CASCADE
);
create table comments_of_questions(
	id int not null primary key auto_increment,
	content varchar(1000) not null,
	author_id int not null,
	date_modify datetime not null,
	question_id int not null,
	foreign key (author_id) references users(id) on delete CASCADE on update CASCADE,
	foreign key (question_id) references questions(id) on delete CASCADE on update CASCADE
);
create table ignore_characters(
	id int not null primary key auto_increment,
	content varchar(40) not null
);
create table books(
	id int not null primary key auto_increment,
	name varchar(500) not null,
	description varchar(4000) not null,
	lesson_id int not null,
	page_number int not null,
	author_id int not null,
	created_date date,
	date_modify date,
	foreign key (lesson_id) references lessons(id) on delete CASCADE on update CASCADE,
	foreign key (author_id) references users(id) on delete CASCADE on update CASCADE
);
create table groups_questions(
	id int not null primary key auto_increment,
	name varchar(500) not null,
	description varchar(4000) not null,
	question_id int not null,
	number_order int not null,
	author_id int not null,
	reated_date date,
	date_modify date,
	foreign key (question_id) references questions(id) on delete CASCADE on update CASCADE,
	foreign key (author_id) references users(id) on delete CASCADE on update CASCADE
)
create table result_test(
	id int not null primary key auto_increment,
	exam_id int not null,
	user_id int not null,
	question_id int not null,
	choose_answer varchar(10) not null,
	foreign key (exam_id) references exams(id) on update CASCADE,
	foreign key (user_id) references users(id) on update CASCADE,
	foreign key (question_id) references questions(id) on update CASCADE
)
create table chart_data(
	user_id int not null,
	category_id int not null,
	subject_id int not null,
	exam_id int not null,
	choose_correct int not null,
	primary key ( user_id, category_id)
)
create view question_of_exam as select 
	eq.id as id, eq.exam_id, eq.question_number as question_number, eq.description as question_desc,
	q.id as question_id, q.question_name, question_content, s.name as subject,
	amc.option_a, amc.option_b, amc.option_c, amc.option_d, amc.option_e, amc.true_answer 
	from exams_questions as eq 
	join questions as q on eq.question_id = q.id
	join  subjects as s on q.subject_id=s.id
	left join answers_of_mul_choise as amc on amc.question_id = q.id;
	
create view question_answer as select
	eq.id as id, eq.exam_id, eq.question_number as question_number, eq.description as question_desc, 
	q.id as question_id, q.question_name, q.question_content, q.question_type, q.difficulty,
	a.answer_content
	from exams_questions as eq
	join questions as q on eq.question_id=q.id
	left join answers as a on q.id=a.question_id;
